/**********************************************************************
* 
* macros.h
*
* This file is part of MitoGen
* 
* Copyright (C) 2013-2016 -- Centre for Biomedical Image Analysis (CBIA)
* http://cbia.fi.muni.cz/
* 
* MitoGen is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* MitoGen is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with MitoGen. If not, see <http://www.gnu.org/licenses/>.
* 
* Author: David Svoboda and Vladimir Ulman
* 
* Description: Definition of handy macros and constants
*
***********************************************************************/

#ifndef MITOGEN_MACROS_H
#define MITOGEN_MACROS_H

//-----------------------------------------------------------------------

#define DEG_TO_RAD(x) ((x)*M_PI/180)
#define SQR(x) ((x)*(x))
#define EVEN(x) (((x) % 2) == 0)
#define ODD(x) (((x) % 2) == 1)

/**
 * intended to evaluate !mask.Include(x,y,z) with [mM][xyz] specifing
 * boundary pixel coordinates to compare with; typically these are 1px
 * or 2px inwards from real boundaries of image in question
 */
#define NOT_INCLUDE(x,mx,Mx, y,my,My, z,mz,Mz)	((x) < (mx)) \
						|| ((x) > (Mx)) \
						|| ((y) < (my)) \
						|| ((y) > (My)) \
						|| ((z) < (mz)) \
						|| ((z) > (Mz))

#include <iostream>
#include <string.h>

#define _SHORT_FILE_ strrchr(__FILE__, '/') ? \
			strrchr(__FILE__, '/') + 1 : __FILE__

/// helper macro to unify reports
#define REPORT(x) std::cout \
   	<< std::string(_SHORT_FILE_) << "::" << std::string(__FUNCTION__) \
	<< "(): " << x << std::endl;

/**
 * helper macro to unify debug reports
 *
 * Reports are suppressed in release version of program.
 */
#ifdef GTGEN_DEBUG
   #define DEBUG_REPORT(x) REPORT(x)
#else
   #define DEBUG_REPORT(x)
#endif

/// helper macro to unify error reports
#define ERROR_REPORT(x) std::string(_SHORT_FILE_)+"::"+__FUNCTION__+"(): "+x

/// helper macro that contains user preferred filename suffix
#ifdef GTGEN_WITH_TIFFS
   #define SUFFIX "tif"
#else
   #define SUFFIX "ics"
#endif

#endif
