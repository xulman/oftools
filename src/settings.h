/**********************************************************************
* 
* settings.h
*
* This file is part of MitoGen
* 
* Copyright (C) 2013-2016 -- Centre for Biomedical Image Analysis (CBIA)
* http://cbia.fi.muni.cz/
* 
* MitoGen is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* MitoGen is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with MitoGen. If not, see <http://www.gnu.org/licenses/>.
* 
* Author: David Svoboda and Vladimir Ulman
* 
* Description: The list of properties of the framework that are
* set during the configuration phase by CMake. This file has no
* special functionality. It is rather a documentation of individual
* parameters.
*
***********************************************************************/

#ifndef MITOGEN_SETTINGS_H
#define MITOGEN_SETTINGS_H

#include "types.h"
#include "macros.h"

/**
 * Whether the simulator should generate images with chromosome
 * territories along with the mask and phantom images.
 * The territories are drawn with the Cell::ChrRenderIntoTerritories() function.
 *
 * In fact, the macro is controlled from the CMake.
 */
//#define GTGEN_WITH_TERRITORIES

/**
 * This macro, if enabled, shows Cell::chrCentres in the phantom images
 * by adding a big constant value to voxels at particular chrCentre positions.
 * That happens only during the Cell::ChrRenderIntoPhantoms().
 *
 * Both PrepareFinalPreviewImage() and PrepareFinalImage() functoins then detect
 * this extraordinary big values and subtract the same big constant so that original
 * phantom value is restored at all voxels before final preview image is created.
 *
 * In fact, the macro is controlled from the CMake.
 */
//#define GTGEN_WITH_CHRCENTRESINPHANTHOMS

/**
 * Whether the simulator should generate mask images with cell
 * nuclei along with the cell mask and phantom images.
 *
 * In fact, the macro is controlled from the CMake.
 */
//#define GTGEN_WITH_NUCLEIMASKS

/**
 * The simulator internally always works with images of isotropic resolution,
 * which is learnt from the configuration file (the -c parameter; z-axis resolution
 * is made equal to the x-axis resolution). All images, except for the final (preview)
 * images, will be saved with this resolution, i.e., isotropic.
 *
 * The only exceptions are the final (preview) images, which are products of the
 * PrepareFinalPreviewImage() and PrepareFinalImage() functions, and which are
 * always (regardless of this macro state) saved in some desired possibly anisotropic
 * resolution (the z-axis resolution is learnt from the configuration file, the
 * "z step" parameter in the "table" section).
 *
 * If this macro is enabled, the phantom and mask images will be additionally, that
 * is, ONLY after the stage III is finished, resampled to become of the same
 * (anisotropic) resolution as are the final images; and saved with nonISO suffix.
 * The simulator can run in two modes: a full simulation that creates also the phantom
 * and mask images, or just stage II and III processing that consumes already existing
 * phantom and mask images. To make this macro effective in the former case, the macro
 * GTGEN_WITH_FINALIMAGES must be enabled as well. For the second, the state of the
 * GTGEN_WITH_FINALIMAGES is irrelevant.
 *
 * In fact, the macro is controlled from the CMake.
 */
//#define GTGEN_WITH_ANISOTROPIC_GT

/**
 * Whether the simulator should also produce final images,
 * which are basically a phantom image disturbed with several sorts of
 * noise. It should simulate a transmission of the phantom image throught
 * optical and sensor systems.
 *
 * In fact, the macro is controlled from the CMake.
 */
//#define GTGEN_WITH_FINALIMAGES

/**
 * If the simulator produces final images, it has to pass the phantom
 * images throught the simulators of both optical and sensor systems.
 *
 * With this macro enabled, the input configuration file (given with
 * the -c parameter) is further considered in order to learn many parameters
 * describing certain real system (described therein), including a path to
 * an image with a real PSF. Especially, the application of the PSF is rather
 * time consuming operation. The quality of produced images is controlled
 * only via the configuration file. The macro GTGEN_WITH_HIGHSNR is ignored.
 * The produced final images are saved under the names ``sceneFinal''.
 *
 * With this macro disabled, the simulator uses its internal presets and
 * approximations to an optical and sensor systems in order to produce final images.
 * This is usually very fast but, of course, of lower quality. In this mode,
 * the simulator disntiquishes between two levels of quality of the images, which
 * is controlled via the macro GTGEN_WITH_HIGHSNR.
 * The produced final images are saved under the names ``sceneFinalPreview''.
 *
 * In fact, the macro is controlled from the CMake.
 */
//#define GTGEN_WITH_REAL_PSF

/**
 * If the simulator produces final previews, i.e., the macro GTGEN_WITH_REAL_PSF is
 * disabled, it can produce them at two qualitative levels, that is, at high or low SNR.
 * This macro, if defined, forces the simulator to choose the high SNR. Otherwise,
 * the low SNR is used.
 *
 * In fact, the macro is controlled from the CMake.
 */
//#define GTGEN_WITH_HIGHSNR

/**
 * Whether the simulator should save the masks, nuclei, phanthoms, final
 * previews, and territories (if the respective images are enabled) with
 * CBIA 3D TIFF image fileformat (it is basically a standard multi-page TIFF
 * format with some custom directory to save a resolution along z-axis;
 * it is scompatible with, e.g., ImageJ). Otherwise, the images are saved
 * with Image Cytometry Standard, ICS image fileformat.
 *
 * In fact, the macro is controlled from the CMake.
 */
//#define GTGEN_WITH_TIFFS

/**
 * Whether the simulator should also save the flow fields describing motion
 * for every cell at every time step. Local internal motions within cells are
 * not saved now.
 *
 * In fact, the macro is controlled from the CMake.
 */
//#define GTGEN_WITH_FLOWFIELDS

/**
 * Whether cells are allowed to perform translation and rotation when they
 * are moving in the environment (in the scene). It does not affect the movements
 * happening inside the cell (aka internal local motions of intracellular structures).
 *
 * In fact, the macro is controlled from the CMake.
 */
//#define GTGEN_WITH_RIGIDMOTION

/**
 * Whether cells are allowed to perform non-rigid deformation of cell body when they
 * are moving in the environment (in the scene). It does not affect the movements
 * happening inside the cell (aka internal local motions of intracellular structures).
 *
 * In fact, the macro is controlled from the CMake.
 */
//#define GTGEN_WITH_NONRIGIDMOTION

/**
 * If this macro is enabled/defined, the translation along z-axis of any cell is
 * suppressed. However, it is not blocked at all.
 *
 * In fact, the macro is controlled from the CMake.
 */
//#define GTGEN_WITH_JUST2DTRANSLATIONS

/**
 * This macro, if enabled, forces mitotic events inside the cell population
 * to happen more or less synchronized, that is, all cells do divide with
 * short period of frames. Normally, the divisions should occur rather randomly.
 *
 * In fact, the macro is controlled from the CMake.
 */
//#define GTGEN_WITH_STRONGSYNCHRONIZATION

/**
 * Whether the phantom images, and final (preview) images consequently as well,
 * shall be subjected to the effect of the photobleaching of a fluorescence material
 * -- an artifact often severely degrading quality of images in live cell microscopy.
 *
 * In fact, the macro is controlled from the CMake.
 */
//#define GTGEN_WITH_PHOTOBLEACHING

/**
 * Certain parts of the simulator routines are programed to support the use of
 * the parallel processing model based on POSIX threads. If this macro is defined,
 * such routines are activated. If it is not, single-thread alternatives take place.
 * Note that this macro does not control usage of parallel computations implemented
 * in the CBIA i3dlibs library.
 *
 * In fact, the macro is controlled from the CMake.
 */
//#define GTGEN_WITH_MULTITHREADING

#endif
