#ifndef MITOGEN_IMPORTEXPORT_H
#define MITOGEN_IMPORTEXPORT_H

/**
 * Reads input mask image \e mask and seeks for voxel with value \e value.
 * The micron coordinates (taking into account the offset and resolution
 * of the input image) are exported into the file given with \e fileName.
 *
 * The format of the export is a white-space separated three float columns
 * representing the x,y, and z coordinates of voxel centres, respectively;
 * a line in the file defines a voxel. 
 *
 * If the \e value is zero, then all non-zero voxels in the \e mask are exported.
 *
 * \param[in] mask 	the mask to be exported
 * \param[in] value	the value that represent masked object of interest
 * \param[in] fileName	filename of the output file
 */
template <class T>
void ExportAllMaskVoxelCoordinates(const i3d::Image3d<T> &mask,
											  const T value,
											  const char* fileName);

/**
 * Reads input mask image \e mask and seeks for voxel with value \e value
 * and a position on a grid. The grid is given with \e step pixels distances
 * between the grid lines; grid starts from (0,0,0) pixel position in the image.
 * Actually, x-axis distance \e step.x being, e.g., 5 (pixels) means that
 * there should be a grid line, 4 pixels, and the next grid line.
 * The micron coordinates (taking into account the offset and resolution
 * of the input image) are exported into the file given with \e fileName.
 *
 * The format of the export is a white-space separated three float columns
 * representing the x,y, and z coordinates of voxel centres, respectively;
 * a line in the file defines a voxel. 
 *
 * If the \e value is zero, then all non-zero voxels in the \e mask are exported.
 *
 * \param[in] mask 	the mask to be exported
 * \param[in] value	the value that represent masked object of interest
 * \param[in] step	the grid lines stepping in pixels
 * \param[in] fileName	filename of the output file
 */
template <class T>
void ExportGridMaskVoxelCoordinates(const i3d::Image3d<T> &mask,
												const T value,
												i3d::Vector3d<size_t> const &step,
												const char* fileName);

/**
 * Reads the input list of points \e PList and exports not more than
 * \e maxCnt points (or the whole list if it is shorter than that)
 * into the file \e fileName. The number of actually written points
 * shall be the return value of the function.
 *
 * The format of the export is a white-space separated three float columns
 * representing the x,y, and z coordinates of voxel centres, respectively;
 * a line in the file defines a voxel. 
 *
 * \param[in] PList		a vector (a list) of points to be exported
 * \param[in] maxCnt		maximum number of points to be exported
 * \param[in] fileName	filename of the output file
 *
 * \return The number of actually written points.
 */
template <class T>
size_t ExportPointList(std::vector< T /**/> const &PList,
							  const size_t maxCnt,
							  const char* fileName);

/**
 * Reads the input file \e fileName with point coordinates and fills
 * the output list of points \e PList. The list is erased before re-filled
 * completely from the scratch. The file format is expected to be the
 * same as in the ExportPointList() function.
 *
 * If the \e Pcentre pointer is not null, an average coordinate of the
 * imported points will be stored there.
 *
 * \param[out] PList		a vector (a list) of points to be imported
 * \param[out] Pcentre	an average coordinate of the imported points
 * \param[in] fileName	filename of the input file
 * \param[in] estCount	(over)estimate of how many points are ought to be read
 *
 * \note One may use the \e estCount parameter to aid the routine, which
 * can reserve adequate buffer, which makes the program run considerably
 * faster and which allows to obtain continuous IDs (in dot lists that support
 * dot IDs). Therefore, a decent overestimate is better than underestimate.
 *
 * \return The number of actually read points; basically, the length
 * of the new point list.
 */
template <class T>
size_t ImportAndResetPointList(std::vector< T /**/> &PList,
										 i3d::Vector3d<float>* Pcentre,
										 const char* fileName,
										 const size_t estCount=0);

/**
 * Reads the input file \e fileName with point coordinates and fills
 * the output list of points \e PList. The list is over-written, meaning
 * no memory management (erase old, allocate new) is occuring, but not
 * more than \e maxCnt points (or the whole list if it is shorter than
 * that). The file format is expected to be the same as in the
 * ExportPointList() function.
 *
 * If the \e Pcentre pointer is not null, an average coordinate of the
 * imported points will be stored there.
 *
 * \param[out] PList		a vector (a list) of points to be imported
 * \param[out] Pcentre	an average coordinate of the imported points
 * \param[in] maxCnt		maximum number of points to be imported
 * \param[in] fileName	filename of the input file
 *
 * \return The number of actually read points.
 */
template <class T>
size_t ImportPointList(std::vector< T /**/> &PList,
							  i3d::Vector3d<float>* Pcentre,
							  const size_t maxCnt,
							  const char* fileName);

#endif
