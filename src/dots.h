/**********************************************************************
* 
* dots.h
*
* This file is part of MitoGen
* 
* Copyright (C) 2013-2016 -- Centre for Biomedical Image Analysis (CBIA)
* http://cbia.fi.muni.cz/
* 
* MitoGen is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* MitoGen is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with MitoGen. If not, see <http://www.gnu.org/licenses/>.
* 
* Author: David Svoboda and Vladimir Ulman
* 
* Description: Definition of class 'Dot' that describes 
* the point-like signals in the specimen.
*
***********************************************************************/

#ifndef MITOGEN_DOTS_H
#define MITOGEN_DOTS_H

#include <i3d/vector3d.h>
#include "settings.h"

/**
 * A helper function to provide IDs for objects which guarantees their uniqueness
 * within the whole scene throughout the whole simulation.
 */
size_t GetNewDotID(void);

/**
 * The basic class describing one particular dot at some time point. The time point is,
 * however, not stored in this class but rather in the Cell class, which is expected
 * to use this class for representation of dot-like structures, e.g. proteins.
 *
 * There is a unique ID assigned to every dot in the scene. This ID is not changing
 * between time points for the given dot as long as the dot exists in the scene. In case the
 * dot is split, e.g. after mitotic cell phase, the parent dot should be removed from the
 * scene and two new dots with new unique IDs will emerge instead. Of course, the new ones
 * will have their \e parentID set appropriately.
 */
class Dot {
  public:
	///empty/default constructor: inits especially positions to (0,0,0)
  	Dot() : pos(0.f,0.f,0.f), nextPos(0.f,0.f,0.f),
	  parentID(0), birth(0) { ID = GetNewDotID(); };

	///"add new dot" constructor: passes its arguments into the class attributes
  	Dot(i3d::Vector3d<float> const & POS, const size_t PARENTID=0, const size_t BIRTH=0) :
	  pos(POS), nextPos(0.f,0.f,0.f), parentID(PARENTID), birth(BIRTH) { ID = GetNewDotID(); };

	///standard "copy" constructor; use the constructor above to add daughter dots 
  	Dot(Dot const & DOT) {
		pos       = DOT.pos;
		nextPos   = DOT.nextPos;
		ID        = DOT.ID;
		parentID  = DOT.parentID;
		birth		 = DOT.birth;
	}

	///destructor: does nothing, currently
	~Dot() {};

	///the current 3D coordinate of the dot, in microns
	i3d::Vector3d<float> pos;

	///future 3D coordinate of the dot, in microns; temporarily used for calculating new positions
	i3d::Vector3d<float> nextPos;

	///an unique non-zero identifier of the dot
	size_t ID;

	/**
	 * an identificator of the dot which represented this one in some previous time point;
	 * this can be used, for example, for tracing positions of a dot
	 *
	 * if \e parentID is 0, it is assumed that this dot does not have any previous representation;
	 * this is typical for dots in the first frame
	 */
	size_t parentID;

	/// time point when this dot started to exist
	size_t birth;
};

#endif
