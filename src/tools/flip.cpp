/*
 * Tools related to the MitoPacq, the CBIA time-lapse cell observation simulator.
 *
 * Copyright (C) 2005-2013   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library-related tools is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <i3d/image3d.h>
#include <i3d/basic.h>
#include <i3d/toolbox.h>
#include <iostream>

using namespace i3d;

template <class VOXEL> void FlipImage(Image3d <VOXEL> &Img, char *SAxe)
{

    size_t i;

    if ((strchr(SAxe, 'x') != NULL) || (strchr(SAxe, 'X') != NULL))
    {
        for (i = 0; i < Img.GetSizeZ(); ++i)
        {
            SwapHoriz(Img.GetSizeX(), Img.GetSizeY(), Img.GetVoxelAddr(i * Img.GetSizeX() * Img.GetSizeY()));
        }
    }
    if ((strchr(SAxe, 'y') != NULL) || (strchr(SAxe, 'Y') != NULL))
    {
        for (i = 0; i < Img.GetSizeZ(); ++i)
        {
            SwapVert(Img.GetSizeX(), Img.GetSizeY(), Img.GetVoxelAddr(i * Img.GetSizeX() * Img.GetSizeY()));
        }
    }

    if ((strchr(SAxe, 'z') != NULL) || (strchr(SAxe, 'Z') != NULL))
    {
        VOXEL *TempBuffer = NULL;
        VOXEL *FirstPtr = NULL;
        VOXEL *LastPtr = NULL;
        size_t sizexy = Img.GetSizeX() * Img.GetSizeY();
        if (Img.GetSizeZ() > 1)
        {
            TempBuffer = new VOXEL[sizexy];
            FirstPtr = Img.GetVoxelAddr(0);
            LastPtr = Img.GetVoxelAddr(Img.GetImageSize()) - sizexy;

            for (i = 0; i < Img.GetSizeZ() / 2; ++i)
            {
                memcpy((void *) TempBuffer, (void *) LastPtr, sizexy * sizeof (VOXEL));
                memcpy((void *) LastPtr, (void *) FirstPtr, sizexy * sizeof (VOXEL));
                memcpy((void *) FirstPtr, (void *) TempBuffer, sizexy * sizeof (VOXEL));
                LastPtr -= sizexy;
                FirstPtr += sizexy;
            }

            delete[]TempBuffer;
        }
    }
}

template <class VOXEL> void DoFlip(char *file_name, char *out_file_name, char *axes)
{
    i3d::Image3d <VOXEL> Img(file_name);
    FlipImage(Img, axes);
    Img.SaveImage(out_file_name);
}

int main(int argc, char **argv)
{
    std::cout << argv[0] << " - part of the tools set of the OpticalFlow library \n"
         "Copyright (C) 2005-2013 Centre for Biomedical Image Analysis (CBIA)\n"
         "This is free software; see the source for copying conditions. "
         "There is NO\nwarranty; not even for MERCHANTABILITY or FITNESS FOR "
         "A PARTICULAR PURPOSE.\n\n";


    if (argc < 4)
    {
        std::cout << "Bad number of parameters\nUsage: " << argv[0] << " input output flip_around_axes\nexample: " << argv[0] << "original.tif flipped.tif yz\n";
        return -2;
    }
    i3d::ImgVoxelType vt = i3d::ReadImageType(argv[1]);
    std::cerr << "Voxel=" << VoxelTypeToString(vt) << std::endl;

    switch (vt)
    {
    case i3d::Gray8Voxel:
        {
            DoFlip <i3d::GRAY8> (argv[1], argv[2], argv[3]);
            break;
        }
    case i3d::Gray16Voxel:
        {
            DoFlip <i3d::GRAY16> (argv[1], argv[2], argv[3]);
            break;
        }
    case i3d::RGBVoxel:
        {
            DoFlip <i3d::RGB> (argv[1], argv[2], argv[3]);
            break;
        }
    case i3d::FloatVoxel:
        {
            DoFlip <float>(argv[1], argv[2], argv[3]);
	    break;
        }
    default:
        std::cerr << "Other voxel types not supported" << std::endl;
    }
    return 0;
}
