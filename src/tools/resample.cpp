/*
 * Tools related to the MitoPacq, the CBIA time-lapse cell observation simulator.
 *
 * Copyright (C) 2005-2013   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library-related tools is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <i3d/image3d.h>
#include <i3d/transform.h>
#ifdef _MSC_VER
#include <time.h>
#else
#include <sys/time.h>
#endif
#include <ctype.h>

using namespace i3d;
using namespace std;

template <class VOXEL> void DoLanczosResample(char *inImage, char *outImage,  float scale[3], int window)
{
    Image3d <VOXEL> *InImg = new Image3d <VOXEL> (inImage);
    Image3d <VOXEL> *OutImg = 0;

    cout << "Performing Lanczos resampling algorithm\n" << endl;
    lanczos_resample(InImg, OutImg, scale, window);

    OutImg->SaveImage(outImage);
    delete InImg;
    delete OutImg;
}

template <class VOXEL> void DoNNResample(char *inImage, char *outImage, const float scale[3])
{
    Image3d <VOXEL> *InImg = new Image3d <VOXEL> (inImage);
    Image3d <VOXEL> *OutImg =  new Image3d <VOXEL> ();
    size_t size[3];
    
    cout << "Performing Nearest Neighbour resampling algorithm\n" << endl;

    for (size_t i = 0 ; i < 3 ; ++i)
    {
	size[i] = static_cast<size_t>( round (scale[i] * static_cast<float>(InImg->GetSize()[i])));

    }
    Resample(*InImg, *OutImg, size[0],size[1],size[2]);
    OutImg->SaveImage(outImage);
    delete InImg;
    delete OutImg;
}



int main(int argc, char **argv)
{
    cout << argv[0] << " - part of the tools set of the OpticalFlow library \n"
         "Copyright (C) 2005-2013 Centre for Biomedical Image Analysis (CBIA)\n"
         "This is free software; see the source for copying conditions. "
         "There is NO\nwarranty; not even for MERCHANTABILITY or FITNESS FOR "
         "A PARTICULAR PURPOSE.\n\n";


    if ((argc != 6) && (argc != 7))
    {
        std::cout << "Bad number of parameters\nUsage: " << argv[0] << " input output x_factor y_factor z_factor [lanczos_window]\nexample: " << argv[0] << " input.ics output.ics 2.0 2.0 1.0 [2|3]\n";
        std::cout << "if lanczos window is not set, nearest neighbour will be used.\n";
        return -2;
    }
    i3d::ImgVoxelType vt = i3d::ReadImageType(argv[1]);
    std::cerr << "Voxel=" << VoxelTypeToString(vt) << std::endl;
    int window = 0;
	if (argc > 6)
	window = atoi(argv[6]);
    float scale[3];
    scale[0] = atof(argv[3]);
    scale[1] = atof(argv[4]);
    scale[2] = atof(argv[5]);

    switch (vt)
    {
    case i3d::Gray8Voxel:
        {

	    if (argc == 7)
		DoLanczosResample <i3d::GRAY8> (argv[1], argv[2], scale, window);
	    else 
		DoNNResample <i3d::GRAY8> (argv[1], argv[2], scale);
            break;
        }
    case i3d::Gray16Voxel:
        {

	    if (argc == 7)
		DoLanczosResample <i3d::GRAY16> (argv[1], argv[2], scale, window);
	    else 
		DoNNResample <i3d::GRAY16> (argv[1], argv[2], scale);
            break;
        }
    case i3d::FloatVoxel:
        {
	    if (argc == 7)
		DoLanczosResample <float>(argv[1], argv[2], scale, window);
	    else
		DoNNResample <float> (argv[1], argv[2], scale);
            break;
        }
    default:
        std::cerr << "Other voxel types not supported" << std::endl;
    }
    return 0;
}
