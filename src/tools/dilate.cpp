/*
 * Tools related to the MitoPacq, the CBIA time-lapse cell observation simulator.
 *
 * Copyright (C) 2005-2013   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library-related tools is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <i3d/image3d.h>
#include <i3d/morphology.h>

using namespace std;
using namespace i3d;

template <class T> I3D_ALGO_EXPORT void DilationO_always2D(const Image3d<T> &in, Image3d<T> &out, const int radius_size);
template <class T> I3D_ALGO_EXPORT void ErosionO_always2D(const Image3d<T> &in, Image3d<T> &out, const int radius_size);


int main(int argc, char **argv)
{
    cout << argv[0] << " - part of the tools set of the OpticalFlow library \n"
         "Copyright (C) 2005-2013 Centre for Biomedical Image Analysis (CBIA)\n"
         "This is free software; see the source for copying conditions. "
         "There is NO\nwarranty; not even for MERCHANTABILITY or FITNESS FOR "
         "A PARTICULAR PURPOSE.\n\n";

    if (argc != 4 && argc != 5)
    {
        cout << "Program performs gray morphology dilate on the input image, saving it into output image.\n";
        cout << "The dilation uses 2D/3D circular structuring element of the given radius. The radius is given\n";
        cout << "in pixels and is applied on all axes the same (as if input was of isotropic resolution).\n";
        cout << "The optional 4th parameter enforces slice-by-slice processing.\n";

        cout << "Bad number of parameters\n" << "Usage: " << argv[0] << " input output pxRadius_Int [y|2d|2D]\n";
        return -2;
    }

    const int radius = atol(argv[3]);
	 const bool sliceBySlice = argc == 5 ? true : false;
	 std::cout << "Circular SE of radius = " << radius << ", sliceBySlice = " << (sliceBySlice ? "yes":"no") << "\n";

    try
    {
        cerr << "Reading image Type" << endl;
        ImgVoxelType vt = ReadImageType(argv[1]);
        cerr << "Voxel=" << VoxelTypeToString(vt) << endl;

        switch (vt)
        {
        case BinaryVoxel:
            {
                 Image3d <bool> Input(argv[1]), Output;
					  if (sliceBySlice)
                   DilationO_always2D(Input,Output,radius);
					  else
                   DilationO(Input,Output,radius);
                 Output.SaveImage(argv[2]);
                 break;
            }
        case Gray8Voxel:
            {
                 Image3d <GRAY8> Input(argv[1]), Output;
					  if (sliceBySlice)
                   DilationO_always2D(Input,Output,radius);
					  else
                   DilationO(Input,Output,radius);
                 Output.SaveImage(argv[2]);
                 break;
            }
         case Gray16Voxel:
            {
                 Image3d <GRAY16> Input(argv[1]), Output;
					  if (sliceBySlice)
                   DilationO_always2D(Input,Output,radius);
					  else
                   DilationO(Input,Output,radius);
                 Output.SaveImage(argv[2]);
                 break;
            }
        default:
            cerr << "Other voxel types not supported" << endl;
        }
    }
    catch(IOException & e)
    {
        cerr << e << endl;
        exit(1);
    }
    catch(InternalException & e)
    {
        cerr << e << endl;
        exit(1);
    }
    catch(bad_alloc &)
    {
        cerr << "Not enough memory." << endl;
        exit(1);
    }
    catch(...)
    {
        cerr << argv[0] << "Unknown exception." << endl;
        exit(1);
    }
    return 0;
}


	template <class T> I3D_ALGO_EXPORT void DilationO_always2D(const Image3d<T> &in, Image3d<T> &out, const int radius_size)
	{
		Image3d<T> tmp;
		tmp = in;

		int i = 0;
		while (i < radius_size) {
			if ((i % 2) == 0) {
				i3d::Dilation(tmp, out, i3d::nb2D_o4);
			} else {
				i3d::Dilation(out, tmp, i3d::nb2D_o8);
			}
			++i;
		}

		if ((radius_size % 2) == 0)
			out = tmp;
	}

	template <class T> I3D_ALGO_EXPORT void ErosionO_always2D(const Image3d<T> &in, Image3d<T> &out, const int radius_size)
	{
		Image3d<T> tmp;
		tmp = in;

		int i = 0;
		while (i < radius_size) {
			if ((i % 2) == 0) {
				i3d::Erosion(tmp, out, i3d::nb2D_o4);
			} else {
				i3d::Erosion(out, tmp, i3d::nb2D_o8);
			}
			++i;
		}

		if ((radius_size % 2) == 0)
			out = tmp;
	}
