/*
 * Tools related to the MitoPacq, the CBIA time-lapse cell observation simulator.
 *
 * Copyright (C) 2005-2013   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library-related tools is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <i3d/image3d.h>
#include <i3d/basic.h>
#include <fstream>
#include <vector>

using namespace i3d;
using namespace std;


int main(int argc, char **argv)
{
    cout << argv[0] << " - part of the tools set of the OpticalFlow library \n"
         "Copyright (C) 2005-2013 Centre for Biomedical Image Analysis (CBIA)\n"
         "This is free software; see the source for copying conditions. "
         "There is NO\nwarranty; not even for MERCHANTABILITY or FITNESS FOR "
         "A PARTICULAR PURPOSE.\n\n";

    if (argc < 3)
    {
        cout << "Bad number of parameters\nUsage: " << argv[0] << " dimension output\n" \
	     << "It reads a sequence of numbers x y x y x y... from STDIN (only integers are accepted),\n" \
	     << "the parameter dimension (1,2 or 3) decides how many numbers describe a coordinate of a point.\n" \
	     << "example: cat body_list | " << argv[0] << " 2 output.vtk\n" ;
        return -2;
    }

    try {
    	int dimen = 0;
	if ( ((dimen=atoi(argv[1])) == 0) || (dimen > 3) ) {
		cout << "Bad number as argument, expecting 1, 2 or 3\n";
		return (-1);
	}

	//now, read all STDIN since we need to know the number of points
	vector<float> coords;
	float tmp;

	while (cin >> tmp) coords.push_back(tmp);
	//coords.size() is the number of points that were really read

	//integrity test #1
	int points=(int)coords.size() / dimen;
	if (points*dimen != (signed)coords.size()) {
		cout << "The number of read points does not match the dimension!\n";
		return (-1);
	}

	//integrity test #2
	if (points == 0) {
		cout << "Zero points detected!\n";
		return (-1);
	}

	fstream filestr;
	filestr.open(argv[2], std::ios::out);

	filestr << "# vtk DataFile Version 3.0\n";
	filestr << "List of point positions.\n";
	filestr << "ASCII\n";

	filestr << "DATASET POLYDATA\n";
	filestr << "POINTS " << points << " int\n";
	for (int i=0; i < points; ++i) {
	  //for (int j=0; j < dimen; ++j)
	  //  filestr << coords[i*dimen + j] << " ";
	  //filestr << endl;
	  filestr << coords[i*dimen+0] << " " \
	       << ((dimen > 1)? coords[i*dimen+1]:0) << " " \
	       << ((dimen > 2)? coords[i*dimen+2]:0) << endl;
	}

	filestr << "\nLINES " << points << " " << points+1 << endl;
	filestr << points << " ";
	for (int i=0; i < points; ++i) filestr << i << " ";
	filestr << endl;

	filestr.close();
    }
    catch(IOException & e)
    {
        cerr << e << endl;
        exit(1);
    }
    catch(InternalException & e)
    {
        cerr << e << endl;
        exit(1);
    }
    catch(bad_alloc &)
    {
        cerr << "Not enough memory." << endl;
        exit(1);
    }
    catch(...)
    {
        cerr << argv[0] << "Unknown exception." << endl;
        exit(1);
    }
    return 0;
}
