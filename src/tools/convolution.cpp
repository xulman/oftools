/*
 * Tools related to the MitoPacq, the CBIA time-lapse cell observation simulator.
 *
 * Copyright (C) 2005-2013   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library-related tools is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <i3d/basic.h>
#include <i3d/image3d.h>
#include <i3d/filters.h>

using namespace i3d;
using namespace std;

int main(int argc, char **argv)
{
    cout << argv[0] << " - part of the tools set of the OpticalFlow library \n"
         "Copyright (C) 2005-2013 Centre for Biomedical Image Analysis (CBIA)\n"
         "This is free software; see the source for copying conditions. "
         "There is NO\nwarranty; not even for MERCHANTABILITY or FITNESS FOR "
         "A PARTICULAR PURPOSE.\n\n";

    if ((argc < 4) || (argc > 5))
    {
        cout << "Bad number of parameters\n" << "usage: " \
	     << argv[0] << " input kernel output [border_value]\n";
	cout << " example: " << argv[0] << " cell_with_noise.ics gaussian.ics cell_filtered.ics 10\n";
	cout << " will extend cell_with_noise.ics with border value 10\n";
        return -2;
    }

    double border=0.0;
    if (argc > 4) border=atof(argv[4]);


    try
    {
        // zjisteni typu vstupniho souboru
        // !!! Nefunguje zjisteni souboru z regularniho vyrazu !!!
        cerr << "Reading image Type" << endl;
        ImgVoxelType vt = ReadImageType(argv[1]);
        cerr << "Voxel=" << VoxelTypeToString(vt) << endl;

        switch (vt)
        {
        case Gray8Voxel:
            {
		cerr << "Can't filter GRAY8 images, only float and double are supported.\n";
                return(-1);
                break;
            }
        case Gray16Voxel:
            {
		cerr << "Can't filter GRAY8 images, only float and double are supported.\n";
                return(-1);
                break;
            }
	case RGBVoxel:
	    {
		cerr << "Can't filter GRAY8 images, only float and double are supported.\n";
                return(-1);
		break;
	    }
	case RGB16Voxel:
	    {
		cerr << "Can't filter GRAY8 images, only float and double are supported.\n";
                return(-1);
		break;
	    }
        case FloatVoxel:
            {
                Image3d <float>i(argv[1]);
		Image3d <float>k(argv[2]);
		Image3d <float>o;

		if (NaiveConvolution(i,k,o,(float)border)) {
			cout << "Convolution didn't passed well, some error occured.\n";
			return(-3);
		}
		o.SaveImage(argv[3]);
                break;
            }
        case DoubleVoxel:
            {
                Image3d <double>i(argv[1]);
		Image3d <double>k(argv[2]);
		Image3d <double>o;

		if (NaiveConvolution(i,k,o,border)) {
			cout << "Convolution didn't passed well, some error occured.\n";
			return(-3);
		}
		o.SaveImage(argv[3]);
                break;
            }
        default:
            cerr << "Other voxel types not supported" << endl;
        }
    }
    catch(IOException & e)
    {
        cerr << e << endl;
        exit(1);
    }
    catch(InternalException & e)
    {
        cerr << e << endl;
        exit(1);
    }
    catch(bad_alloc &)
    {
        cerr << "Not enough memory." << endl;
        exit(1);
    }
    catch(...)
    {
        cerr << argv[0] << "Unknown exception." << endl;
        exit(1);
    }

    return 0;
}
