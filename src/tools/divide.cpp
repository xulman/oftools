/*
 * Tools related to the MitoPacq, the CBIA time-lapse cell observation simulator.
 *
 * Copyright (C) 2005-2013   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library-related tools is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <i3d/image3d.h>
#include <i3d/basic.h>

using namespace i3d;
using namespace std;

template <class VOXEL> void DivideImage(Image3d <VOXEL> &input, Image3d <VOXEL> &output, double divider)
{
    valarray <VOXEL> &ina = input.GetVoxelData();
    valarray <VOXEL> &outa = output.GetVoxelData();
    const size_t amax = ina.size();

    for (size_t i = 0; i < amax; ++i)
    {
        outa[i] = static_cast <VOXEL> (static_cast <double>(ina[i]) / divider);
    }
}

int main(int argc, char **argv)
{
    cout << argv[0] << " - part of the tools set of the OpticalFlow library \n"
         "Copyright (C) 2005-2013 Centre for Biomedical Image Analysis (CBIA)\n"
         "This is free software; see the source for copying conditions. "
         "There is NO\nwarranty; not even for MERCHANTABILITY or FITNESS FOR "
         "A PARTICULAR PURPOSE.\n\n";


    if (argc < 4)
    {
        cout << "Bad number of parameters\nUsage: " << argv[0] << " input output divider\nexample: " << argv[0] << " input.tif output.tif 4\n";
        return -2;
    }
    i3d::ImgVoxelType vt = i3d::ReadImageType(argv[1]);
    std::cerr << "Voxel=" << VoxelTypeToString(vt) << std::endl;

    try
    {
        switch (vt)
        {
        case Gray8Voxel:
            {
                Image3d <GRAY8> input(argv[1]);
                DivideImage <GRAY8> (input, input, atof(argv[3]));
                input.SaveImage(argv[2]);
            }
            break;
        case FloatVoxel:
            {
                Image3d <float>input(argv[1]);
                DivideImage <float>(input, input, atof(argv[3]));
                input.SaveImage(argv[2]);
            }
            break;
        default:
            cerr << "Other voxel types not supported" << endl;
            break;
        }
    }
    catch(IOException & e)
    {
        cerr << e << endl;
        exit(1);
    }
    catch(InternalException & e)
    {
        cerr << e << endl;
        exit(1);
    }
    catch(bad_alloc &)
    {
        cerr << "Not enough memory." << endl;
        exit(1);
    }
    catch(...)
    {
        cerr << argv[0] << "Unknown exception." << endl;
        exit(1);
    }
    return 0;
}
