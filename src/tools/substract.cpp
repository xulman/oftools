/*
 * Tools related to the MitoPacq, the CBIA time-lapse cell observation simulator.
 *
 * Copyright (C) 2005-2013   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library-related tools is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <i3d/image3d.h>
#include <i3d/basic.h>

using namespace i3d;
using namespace std;

template <class VOXEL> void ImageSub(Image3d <VOXEL> &inout, Image3d <VOXEL> &in2)
{
    valarray <VOXEL> &vainout = inout.GetVoxelData();
    valarray <VOXEL> &vain2 = in2.GetVoxelData();

    /*          for(size_t i = 0; i < vainout.size(); ++i)
       {
       if (std::abs(std::abs(vainout[i]) - std::abs(vain2[i]) ) > 0.2)
       {
       cout << i << ": " <<  vainout[i] << " == " <<  vain2[i] << endl;
       vainout[i] = 500;
       }
       else
       vainout[i] = 0;
       } */
    vainout -= vain2;
}

int main(int argc, char **argv)
{
    cout << argv[0] << " - part of the tools set of the OpticalFlow library \n"
         "Copyright (C) 2005-2013 Centre for Biomedical Image Analysis (CBIA)\n"
         "This is free software; see the source for copying conditions. "
         "There is NO\nwarranty; not even for MERCHANTABILITY or FITNESS FOR "
         "A PARTICULAR PURPOSE.\n\n";


    Image3d <float>FImg;
    Image3d <float>FImg1;

    if (argc < 4)
    {
        cout << "Bad number of parameters\nUsage: " << argv[0] << " input0 input1 output\nexample: " << argv[0] << " first.mhd second.mhd result.mhd\n";
        return -2;
    }
    i3d::ImgVoxelType vt = i3d::ReadImageType(argv[1]);
    std::cerr << "Voxel=" << VoxelTypeToString(vt) << std::endl;

    if (vt != i3d::FloatVoxel)
    {
        std::cerr << "Only float voxel type supported" << std::endl;
        std::cerr << "Other voxel types not supported" << std::endl;
    }

    FImg.ReadImage(argv[1]);
    FImg1.ReadImage(argv[2]);
    ImageSub(FImg, FImg1);

    float Min, Max;
    FImg.GetRange(Min, Max);
    cout << "Range of the substraction results: <" << Min << ", " << Max << ">\n";

    FImg.SaveImage(argv[3]);
    return 0;
}
