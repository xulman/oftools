/*
 * Tools related to the MitoPacq, the CBIA time-lapse cell observation simulator.
 *
 * Copyright (C) 2005-2013   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library-related tools is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <iostream>
#include <i3d/image3d.h>


using std::cout;
using std::endl;

template <class MT>
void Merge(i3d::Image3d<MT> &left, i3d::Image3d<MT> const &right) {
	MT *pl=left.GetFirstVoxelAddr();
	const MT *plL=left.GetFirstVoxelAddr() + left.GetImageSize();
	const MT *pr=right.GetFirstVoxelAddr();

	while (pl != plL) {
		*pl = std::max(*pl,*pr);
		++pl; ++pr;
	}
}

template <class MT>
void Merge(i3d::Image3d<MT> &left, 	i3d::Image3d<MT> const &middle,
												i3d::Image3d<MT> const &right) {
	MT *pl=left.GetFirstVoxelAddr();
	const MT *plL=left.GetFirstVoxelAddr() + left.GetImageSize();
	const MT *pm=middle.GetFirstVoxelAddr();
	const MT *pr=right.GetFirstVoxelAddr();

	while (pl != plL) {
		*pl = std::max(*pl,std::max(*pm,*pr));
		++pl; ++pm; ++pr;
	}
}

int main(int argc,char **argv) {

	cout << argv[0] << " - part of the tools set of the OpticalFlow library \n"
		"Copyright (C) 2005-2013 Centre for Biomedical Image Analysis (CBIA)\n"
		"This is free software; see the source for copying conditions. "
		"There is NO\nwarranty; not even for MERCHANTABILITY or FITNESS FOR "
		"A PARTICULAR PURPOSE.\n\n";

	if ((argc != 4) && (argc != 5) && (argc != 6)) {
		cout << "Program merges (=pixel-wise maximum) two images into a third one,\n"
			"or two/three xy-slices of one image into a new one.\n\n";

		cout << "Bad number of parameters\n" << "Usage: " << argv[0]
			<< " inputA inputB output\n  or\nUsage: " << argv[0]
			<< " input sliceA sliceB output\n  or\nUsage: " << argv[0]
			<< " input sliceA sliceB sliceC output\n";
        return -2;
	}
	
	//type of mask images
	typedef i3d::GRAY16 MV;

	if (argc == 4) {
		//merging two images
		i3d::Image3d<MV> left(argv[1]);
		i3d::Image3d<MV> right(argv[2]);

		if (left.GetSize() != right.GetSize()) {
			cout << "sizes of both images do not match\n";
			return(-3);
		}

		Merge(left,right);
		left.SaveImage(argv[3]);
	} else if (argc == 5) {
		//cutting two planes and merging them
		i3d::Image3d<MV> img(argv[1]);
		const int A=(int)atoi(argv[2]);
		const int B=(int)atoi(argv[3]);
		if ((A < 0) || ((size_t)A >= img.GetSizeZ())
			|| (B < 0) || ((size_t)B >= img.GetSizeZ())) {
				cout << "invalid selected z-slice\n";
				return(-3);
		}

		i3d::Image3d<MV> left,right;
		 left.CopyFromVOI(img,i3d::VOI<i3d::PIXELS>(0,0,(size_t)A,img.GetSizeX(),img.GetSizeY(),1));
		right.CopyFromVOI(img,i3d::VOI<i3d::PIXELS>(0,0,(size_t)B,img.GetSizeX(),img.GetSizeY(),1));

		Merge(left,right);
		left.SaveImage(argv[4]);
	} else {
		//cutting three planes and merging them
		i3d::Image3d<MV> img(argv[1]);
		const int A=(int)atoi(argv[2]);
		const int B=(int)atoi(argv[3]);
		const int C=(int)atoi(argv[4]);
		if ((A < 0) || ((size_t)A >= img.GetSizeZ())
			|| (B < 0) || ((size_t)B >= img.GetSizeZ())
			|| (C < 0) || ((size_t)C >= img.GetSizeZ())) {
				cout << "invalid selected z-slice\n";
				return(-3);
		}

		i3d::Image3d<MV> left,middle,right;
		  left.CopyFromVOI(img,i3d::VOI<i3d::PIXELS>(0,0,(size_t)A,img.GetSizeX(),img.GetSizeY(),1));
		middle.CopyFromVOI(img,i3d::VOI<i3d::PIXELS>(0,0,(size_t)B,img.GetSizeX(),img.GetSizeY(),1));
		 right.CopyFromVOI(img,i3d::VOI<i3d::PIXELS>(0,0,(size_t)C,img.GetSizeX(),img.GetSizeY(),1));

		Merge(left,middle,right);
		left.SaveImage(argv[5]);
	}


	return(0);
}
