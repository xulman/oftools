/*
 * Tools related to the MitoPacq, the CBIA time-lapse cell observation simulator.
 *
 * Copyright (C) 2005-2013   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library-related tools is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <i3d/image3d.h>
#include <i3d/basic.h>

using namespace i3d;
using namespace std;

void BinaryToFloat(Image3d<bool> bin,Image3d<float> fout)
{
	fout.MakeRoom(bin.GetSize());
	fout.SetResolution(bin.GetResolution());
	fout.SetOffset(bin.GetOffset());

	bool *binP=bin.GetFirstVoxelAddr();
	float *foutP=fout.GetFirstVoxelAddr();
	for (unsigned int i=0; i < bin.GetImageSize(); ++i, ++binP, ++foutP)
		*foutP=static_cast<float>(*binP) *100.0f;
}

int main(int argc, char **argv)
{
    cout << argv[0] << " - part of the tools set of the OpticalFlow library \n"
         "Copyright (C) 2005-2013 Centre for Biomedical Image Analysis (CBIA)\n"
         "This is free software; see the source for copying conditions. "
         "There is NO\nwarranty; not even for MERCHANTABILITY or FITNESS FOR "
         "A PARTICULAR PURPOSE.\n\n";

    Image3d <float>FImg;

    if (argc < 3)
    {
	cout << "The program converts binary/gray8/gray16 images to float. If binary input image\n";
	cout << "is supplied, it is converted with the \"true\" values set to the value of 100.0.\n\n";

        cout << "Bad number of parameters\nUsage: " << argv[0] << " input output\nexample: " << argv[0] << " grayimage.tif floatimage.mhd\n";
        return -2;
    }

    i3d::ImgVoxelType vt = i3d::ReadImageType(argv[1]);
    std::cerr << "Voxel=" << VoxelTypeToString(vt) << std::endl;

    try
    {
	switch (vt)
	{
	    case BinaryVoxel:
		    {
		    Image3d<bool> Bimg(argv[1]);
		    BinaryToFloat(Bimg,FImg);
		    FImg.SaveImage(argv[2]);
		    }
		    break;

	    case Gray8Voxel:
		{
		    Image3d <i3d::GRAY8> Img (argv[1]);
		    GrayToFloat(Img, FImg);
		    FImg.SaveImage(argv[2]);
		    break;
		}
	    case Gray16Voxel:
		{
		    Image3d <i3d::GRAY16> Img(argv[1]);
		    GrayToFloat(Img, FImg);
		    FImg.SaveImage(argv[2]);
		    break;
		}
	    default:
		std::cerr << "Other voxel types not supported" << std::endl;
		break;

	}
    }
    catch(IOException & e)
    {
        cerr << e << endl;
        exit(1);
    }
    catch(InternalException & e)
    {
        cerr << e << endl;
        exit(1);
    }
    catch(bad_alloc &)
    {
        cerr << "Not enough memory." << endl;
        exit(1);
    }
    catch(...)
    {
        cerr << argv[0] << "Unknown exception." << endl;
        exit(1);
    }
    return 0;
}
