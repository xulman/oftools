/*
 * Tools related to the MitoPacq, the CBIA time-lapse cell observation simulator.
 *
 * Copyright (C) 2005-2013   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library-related tools is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <i3d/image3d.h>
#include <i3d/lut.h>

using namespace std;
using namespace i3d;

template <class VOXEL> void Equalize(Image3d <VOXEL> &Img)
{
    VOXEL iMin, iMax;
    i3d::LUT<VOXEL,VOXEL> lut;

    lut.Init();

    Img.GetRange(iMin,iMax);
    lut.CreateLinearMapping (iMin,iMax);

    lut.Apply(Img.GetVoxelAddr(0), Img.GetVoxelAddr(0	), Img.GetImageSize());

}

int main(int argc, char **argv)
{
    cout << argv[0] << " - part of the tools set of the OpticalFlow library \n"
         "Copyright (C) 2005-2013 Centre for Biomedical Image Analysis (CBIA)\n"
         "This is free software; see the source for copying conditions. "
         "There is NO\nwarranty; not even for MERCHANTABILITY or FITNESS FOR "
         "A PARTICULAR PURPOSE.\n\n";


    if (argc < 3)
    {
        std::cout << "Bad number of parameters\nUsage: " << argv[0] << " input output\nexample: " << argv[0] << " image.tif out.tif\n";
        return -2;
    }
    i3d::ImgVoxelType vt = i3d::ReadImageType(argv[1]);
    std::cerr << "Voxel=" << VoxelTypeToString(vt) << std::endl;

    switch (vt)
    {
    case i3d::Gray8Voxel:
        {
            i3d::Image3d <GRAY8> Img(argv[1]);
            Equalize(Img);
	    Img.SaveImage(argv[2]);
        }
        break;
    case i3d::Gray16Voxel:
        {
            i3d::Image3d <GRAY16> Img(argv[1]);
            Equalize(Img);
	    Img.SaveImage(argv[2]);
        }
        break;
 
    default:
        std::cerr << "Other voxel types not supported" << std::endl;
    }
    return 0;
}
