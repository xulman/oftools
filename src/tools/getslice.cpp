/*
 * Tools related to the MitoPacq, the CBIA time-lapse cell observation simulator.
 *
 * Copyright (C) 2005-2013   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library-related tools is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifdef _MSC_VER
#if _MSC_VER == 1400
#define _CRT_SECURE_NO_DEPRECATE 1
#endif
#endif

#include <i3d/image3d.h>
#include <i3d/basic.h>

using namespace i3d;
using namespace std;

int main(int argc, char **argv)
{
    cout << argv[0] << " - part of the tools set of the OpticalFlow library \n"
         "Copyright (C) 2005-2013 Centre for Biomedical Image Analysis (CBIA)\n"
         "This is free software; see the source for copying conditions. "
         "There is NO\nwarranty; not even for MERCHANTABILITY or FITNESS FOR "
         "A PARTICULAR PURPOSE.\n\n";

    Image3d <GRAY8> Img;    //Input
    Image3d <GRAY8> *Slice;
    if (argc < 4)
    {
        cout << "Bad number of parameters\nUsage: " << argv[0] \
	     << " input  output slice_number\nexample: " << argv[0] \
	     << " image.ics out.tif 20\n";
        return -2;
    }

    /* read the input frames and convert them to float images */
    Img.ReadImage(argv[1]);
    VOI <PIXELS> voi;
    voi.size.x = Img.GetSizeX();
    voi.size.y = Img.GetSizeY();
    voi.size.z = 1;
    voi.offset.x = 0;
    voi.offset.y = 0;
    voi.offset.z = atoi(argv[3]);
    Slice = new Image3d <GRAY8> (Img, &voi);
    Slice->SaveImage(argv[2]);
    delete Slice;
    return 0;
}
