/*
 * Tools related to the MitoPacq, the CBIA time-lapse cell observation simulator.
 *
 * Copyright (C) 2005-2013   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library-related tools is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <i3d/image3d.h>
#include <i3d/vector3d.h>
#include <iostream>

using namespace i3d;

template <class VOXEL> void ReadVector(Vector3d<size_t> &pos,
			Image3d<VOXEL> &FFx,
			Image3d<VOXEL> &FFy,
			Image3d<VOXEL> &FFz)
{
	Vector3d<VOXEL> res(FFx.GetVoxel(pos),FFy.GetVoxel(pos),FFz.GetVoxel(pos));
	std::cout << "\nVector @ " << pos << " is:\n";
	std::cout << "  based on stored values:\n";
	std::cout << "    " << res << ", length is " << Norm(res) << " (microns in gtgen-ng)\n";
	std::cout << "  based on translated values, using resolution " << FFx.GetResolution().GetRes() << " px/um:\n";
	std::cout << "    " << MicronsToPixels(res,FFx.GetResolution()) << ", length is "
		  << Norm(MicronsToPixels(res,FFx.GetResolution())) << " (pixels in gtgen-ng)\n";
	std::cout << "  colour code based on stored values is   TODO\n";
}

int main(int argc, char **argv)
{
    std::cout << argv[0] << " - part of the tools set of the OpticalFlow library \n"
         "Copyright (C) 2005-2013 Centre for Biomedical Image Analysis (CBIA)\n"
         "This is free software; see the source for copying conditions. "
         "There is NO\nwarranty; not even for MERCHANTABILITY or FITNESS FOR "
         "A PARTICULAR PURPOSE.\n\n";


    if (argc < 7)
    {
        std::cout << "Bad number of parameters\nUsage: " << argv[0] << " x y z FFx FFy FFz\n";
	std::cout << "example: " << argv[0] << " 97 95 45 FlowField_x.ics FlowField_y.ics FlowField_z.ics\n";
        return -2;
    }
    i3d::ImgVoxelType vt = i3d::ReadImageType(argv[4]);
    std::cerr << "Voxel=" << VoxelTypeToString(vt) << std::endl;

    i3d::Vector3d<size_t> pos(atol(argv[1]),atol(argv[2]),atol(argv[3]));

    switch (vt)
    {
/*
    case i3d::Gray8Voxel:
        {
	    Image3d<i3d::GRAY8> FFx(argv[4]);
	    Image3d<i3d::GRAY8> FFy(argv[5]);
	    Image3d<i3d::GRAY8> FFz(argv[6]);
            ReadVector<i3d::GRAY8> (pos, FFx,FFy,FFz);
            break;
        }
    case i3d::Gray16Voxel:
        {
	    Image3d<i3d::GRAY16> FFx(argv[4]);
	    Image3d<i3d::GRAY16> FFy(argv[5]);
	    Image3d<i3d::GRAY16> FFz(argv[6]);
            ReadVector <i3d::GRAY16> (pos, FFx,FFy,FFz);
            break;
        }
*/
    case i3d::FloatVoxel:
        {
	    Image3d<float> FFx(argv[4]);
	    Image3d<float> FFy(argv[5]);
	    Image3d<float> FFz(argv[6]);
            ReadVector <float>(pos, FFx,FFy,FFz);
            break;
        }
    case i3d::DoubleVoxel:
        {
	    Image3d<double> FFx(argv[4]);
	    Image3d<double> FFy(argv[5]);
	    Image3d<double> FFz(argv[6]);
            ReadVector <double>(pos, FFx,FFy,FFz);
            break;
        }
    default:
        std::cerr << "Other voxel types not supported" << std::endl;
    }
    return 0;
}
