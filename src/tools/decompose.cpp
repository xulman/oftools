/*
 * Tools related to the MitoPacq, the CBIA time-lapse cell observation simulator.
 *
 * Copyright (C) 2005-2013   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library-related tools is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <i3d/image3d.h>
#include <i3d/basic.h>
#include <iostream>

template <class VOXEL, class VOXELdecomposed> void DoDecompose(char *xyz_name, char *x_name, char *channel)
{
	i3d::Image3d <VOXEL> XYZImg(xyz_name); //input
	i3d::Image3d <VOXELdecomposed> XImg; //output

	XImg.CopyMetaData(XYZImg);

	switch (*channel)
	{
	case 'r':
	case 'R':
		for (size_t i = 0; i < XYZImg.GetImageSize(); ++i)
			XImg.SetVoxel(i, XYZImg.GetVoxel(i).red);
		break;
	case 'g':
	case 'G':
		for (size_t i = 0; i < XYZImg.GetImageSize(); ++i)
			XImg.SetVoxel(i, XYZImg.GetVoxel(i).green);
		break;
	case 'b':
	case 'B':
		for (size_t i = 0; i < XYZImg.GetImageSize(); ++i)
			XImg.SetVoxel(i, XYZImg.GetVoxel(i).blue);
		break;
	default:
		std::cerr << "Couldn't recognize channel " << *channel << ".\n";
	}

	XImg.SaveImage(x_name);
}


int main(int argc, char **argv)
{
    std::cout << argv[0] << " - part of the tools set of the OpticalFlow library \n"
         "Copyright (C) 2005-2013 Centre for Biomedical Image Analysis (CBIA)\n"
         "This is free software; see the source for copying conditions. "
         "There is NO\nwarranty; not even for MERCHANTABILITY or FITNESS FOR "
         "A PARTICULAR PURPOSE.\n\n";


    if (argc != 4)
    {
        std::cout << "Bad number of parameters\n";
	std::cout << "Usage: " << argv[0] << " rgb_input output r|g|b\n";
	std::cout << "example: " << argv[0] << " rgb.ics g.ics g\n";
	std::cout << "The program extracts given colour channel from the input image.\n";
        return -2;
    }
    i3d::ImgVoxelType vt = i3d::ReadImageType(argv[1]);
    std::cerr << "Voxel=" << VoxelTypeToString(vt) << std::endl;

    switch (vt)
    {
    case i3d::RGBVoxel:
        {
            DoDecompose <i3d::RGB,i3d::GRAY8> (argv[1], argv[2], argv[3]);
            break;
        }
    case i3d::RGB16Voxel:
        {
            DoDecompose <i3d::RGB16,i3d::GRAY16> (argv[1], argv[2], argv[3]);
            break;
        }
    default:
        std::cerr << "Other voxel types not supported" << std::endl;
    }
    return 0;
}
