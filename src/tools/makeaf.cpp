/*
 * Tools related to the MitoPacq, the CBIA time-lapse cell observation simulator.
 *
 * Copyright (C) 2005-2013   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library-related tools is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <i3d/image3d.h>
#include <i3d/transform.h>
#include <iostream>
#ifdef _MSC_VER
#include <time.h>
#else
#include <sys/time.h>
#endif
#include <ctype.h>

using namespace i3d;

template <class VOXEL> void DoAF(char *inImage, char *outImage, char *axe)
{
    Image3d <VOXEL> InImg(inImage);
    Image3d <VOXEL> OutImg;

    switch (axe[0])
    {
    case 'x':
    case 'X':
        Get_AF_YZ(InImg, OutImg);
        break;
    case 'y':
    case 'Y':
        Get_AF_XZ(InImg, OutImg);
        break;
    case 'z':
    case 'Z':
        Get_AF_XY(InImg, OutImg);
        break;
    default:
        Get_AF_XY(InImg, OutImg);
        break;
    }
    OutImg.SaveImage(outImage);
}

int main(int argc, char **argv)
{
    std::cout << argv[0] << " - part of the tools set of the OpticalFlow library \n"
         "Copyright (C) 2005-2013 Centre for Biomedical Image Analysis (CBIA)\n"
         "This is free software; see the source for copying conditions. "
         "There is NO\nwarranty; not even for MERCHANTABILITY or FITNESS FOR "
         "A PARTICULAR PURPOSE.\n\n";


    if (argc < 4)
    {
        std::cout << "Bad number of parameters\nUsage: " << argv[0] << " input output projection_axe\nexample: " << argv[0] << " full3d.ics af_projection.tif z\n";
        return -2;
    }
    i3d::ImgVoxelType vt = i3d::ReadImageType(argv[1]);
    std::cerr << "Voxel=" << VoxelTypeToString(vt) << std::endl;

    switch (vt)
    {
    case i3d::Gray8Voxel:
        {
            DoAF <i3d::GRAY8> (argv[1], argv[2], argv[3]);
            break;
        }
    case i3d::Gray16Voxel:
        {
            DoAF <i3d::GRAY16> (argv[1], argv[2], argv[3]);
            break;
        }
    case i3d::FloatVoxel:
        {
            DoAF <float>(argv[1], argv[2], argv[3]);
            break;
        }
    default:
        std::cerr << "Other voxel types not supported" << std::endl;
    }
    return 0;
}
