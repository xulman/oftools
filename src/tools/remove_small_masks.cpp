/*
 * Tools related to the MitoPacq, the CBIA time-lapse cell observation simulator.
 *
 * Copyright (C) 2005-2013   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library-related tools is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <iostream>
#include <i3d/image3d.h>


using std::cout;
using std::endl;


template <class VT>
void Relabel(i3d::Image3d<VT> &img,const VT Lout,const VT Lin) {
	VT *p=img.GetFirstVoxelAddr();
	const VT *pL=img.GetFirstVoxelAddr() + img.GetImageSize();

	while (p != pL) {
		if (*p == Lout) *p=Lin;
		++p;
	}
}

int main(int argc, char **argv) {
    cout << argv[0] << " - part of the tools set of the OpticalFlow library \n"
         "Copyright (C) 2005-2013 Centre for Biomedical Image Analysis (CBIA)\n"
         "This is free software; see the source for copying conditions. "
         "There is NO\nwarranty; not even for MERCHANTABILITY or FITNESS FOR "
         "A PARTICULAR PURPOSE.\n\n";

	//type of mask images
	typedef i3d::GRAY16 MV;

	if (argc == 1) {
		cout << "Usage: " << argv[0] << " min_size input output\n"
			"to remove all masks of volume less than 'min_size' from 'input' image\n"
			"and save the result into the 'output' image\n";
		return(-1);
	}

	const int minSize=atoi(argv[1]);

	//read inputs, mask first then phantom
	cout << "processing mask " << argv[2] << endl;
	i3d::Image3d<MV> mask(argv[2]);

	int hist[65535];
	for (int i=0; i < 65535; ++i) hist[i]=0;

	const MV *pm=mask.GetFirstVoxelAddr();
	const MV *pmL=mask.GetFirstVoxelAddr() + mask.GetImageSize();

	while (pm != pmL) {
		if (*pm) ++hist[*pm];
		++pm;
	}

	cout << "velikosti:\n";
	for (int i=0; i < 65535; ++i) {
		if (hist[i]) cout << hist[i] << " pro ID " << i << endl;
		if ((hist[i] > 0) && (hist[i] < minSize)) {
			cout << "removing ID " << i << endl;
			Relabel(mask,(MV)i,(MV)0);
		}
	}

	mask.SaveImage(argv[3]);

	return(0);
}
