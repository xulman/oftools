/*
 * Tools related to the MitoPacq, the CBIA time-lapse cell observation simulator.
 *
 * Copyright (C) 2005-2013   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library-related tools is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <iostream>
#include <i3d/image3d.h>
#include <vector>


using std::cout;
using std::endl;


int main(int argc, char **argv) {

	cout <<"# " << argv[0] << " - part of the tools set of the OpticalFlow library \n"
		"# Copyright (C) 2005-2013 Centre for Biomedical Image Analysis (CBIA)\n"
		"# This is free software; see the source for copying conditions. "
		"There is NO\n# warranty; not even for MERCHANTABILITY or FITNESS FOR "
		"# A PARTICULAR PURPOSE.\n\n";

	if ((argc != 4) && (argc != 5)) {
		cout << "Program assumes a sequence of (labelled) masks is given to it. It then,\n"
			"for every z-coordinate, counts how many non-zero pixels are there over the\n"
			"whole sequence, and prints such histogram at the end.\n\n";

		cout << "Bad number of parameters\n" << "Usage: " << argv[0]
			<< " filename_pattern from to [reference]\n"
			"where filename_pattern is given directly, as is, to the sprintf() and is\n"
			"expected to obtain exactly one '%03d' directive, and where the from and to\n"
			"define the sequence to be examined; if the reference is given, the histogram\n"
			"from that frame is subtracted from histograms for all frames\n"
			"Example:" << argv[0] << " sceneMasks%03d.tif 0 99\n  or\n"
			"Example:" << argv[0] << " sceneMasks%03d.tif 0 99 2\n";
			
		return -2;
	}

	//type of mask images
	typedef i3d::GRAY16 MV;

	//the output histogram per frame
	std::vector<int> hist;

	//reference histogram
	std::vector<int> hist_reference;

	//frame intervals...
	const size_t countFrom=atoi(argv[2]);
	const size_t countTo=atoi(argv[3]);
	const size_t referenceNo= (argc == 5)? atoi(argv[4]) : countFrom;

	cout << "# frame\tslice\tcount\n";

	//open input reference mask
	char fn[50];
	sprintf(fn,argv[1],referenceNo);
	cout << "# reading: " << fn << "\n";
	i3d::Image3d<MV> frame(fn);

	//for the consistency checks of the input sequence
	const i3d::Vector3d<size_t> dims(frame.GetSize());

	hist.resize(dims.z);
	
	//if the reference parameter is given...
	if (argc == 5) {
		hist_reference.resize(dims.z);
		for (size_t z=0; z < dims.z; ++z) hist_reference[z]=0;

		const MV *p=frame.GetFirstVoxelAddr();
		for (size_t z=0; z < dims.z; ++z)
			for (size_t y=0; y < dims.y; ++y)
				for (size_t x=0; x < dims.x; ++x, ++p)
					if (*p > 0) ++hist_reference[z];
	}


	//now, the main loop: computing histograms for every frame
	for (size_t T=countFrom; T <= countTo; ++T) {
		//open input mask
		sprintf(fn,argv[1],T);
		cout << "\n# reading: " << fn << endl;
		frame.ReadImage(fn);

		//consistency check
		if (frame.GetSize() != dims) {
			cout << "incorrect size\n";
			return(-3);
		}

		//initiate the histogram:
		for (size_t z=0; z < dims.z; ++z) hist[z]=0;

		//update the histogram:
		const MV *p=frame.GetFirstVoxelAddr();
		for (size_t z=0; z < dims.z; ++z)
			for (size_t y=0; y < dims.y; ++y)
				for (size_t x=0; x < dims.x; ++x, ++p)
					if (*p > 0) ++hist[z];


		size_t maxZ=0;
		int maxHist=-50000;

		//for mean and variance calculation
		size_t n=0;
		double avg=0,var=0;

		//report the histogram:
		if (argc < 5) {
			//the histogram as is
			for (size_t z=0; z < dims.z; ++z) {
				cout << T << "\t" << z << "\t" << hist[z] << "\n";
				if (hist[z] > maxHist) {
					maxHist=hist[z];
					maxZ=z;
				}

				//for mean calculation
				avg += (double)hist[z] * (double)z;
				n   +=         hist[z];
			}
			avg /= (double)n;

			//variance calculation
			for (size_t z=0; z < dims.z; ++z) {
				var += (double)hist[z] * ((double)z-avg)*((double)z-avg);
			}
			var /= (double)n;

		} else {
			//the histogram minus reference one
			for (size_t z=0; z < dims.z; ++z) {
				cout << T << "\t" << z << "\t" << hist[z]-hist_reference[z] << "\n";
				if ( (hist[z]-hist_reference[z]) > maxHist) {
					maxHist=hist[z]-hist_reference[z];
					maxZ=z;
				}

				//for mean calculation
				avg += (double)(hist[z]-hist_reference[z]) * (double)z;
				n   +=          hist[z]-hist_reference[z];
			}
			avg /= (double)n;

			//variance calculation
			for (size_t z=0; z < dims.z; ++z) {
				var += (double)(hist[z]-hist_reference[z]) * ((double)z-avg)*((double)z-avg);
			}
			var /= (double)n;
		}

		cout << "# max: " << T << "\t" << maxZ << "\t" << maxHist << endl;
		cout << "# mean&std: " << T << "\t" << avg  << "\t" << sqrt(var) << endl;
	}

	return(0);
}
