#include <iostream>
#include <stdlib.h>


template <typename V>
void process(const int smoothWidth)
{
	const int bufferLength = 2*smoothWidth+1;
	V buffer[bufferLength];

	int c = 0;
	float in;
	V val, newVal;

	while (std::cin >> in)
	{
		val = (V)in;
		buffer[c++ %bufferLength] = val;

		//first run?
		if (c == 1)
		{
			//populate the full buffer with the first value
			for (int i=1; i <= smoothWidth; ++i) buffer[i] = val;
			c += smoothWidth;
		}

		//seen enough values?
		if (c >= bufferLength)
		{
			//average value of the buffer is the output
			newVal = 0;
			for (int i=0; i < bufferLength; ++i) newVal += buffer[i];
			newVal /= (V)bufferLength;
			std::cout << newVal << "\n";
		}
	}

	//finish the remaining output values to provide output of the same
	//length as the input by pretending we are reading the last input
	//value again smoothWidth-times
	val = buffer[(c-1) %bufferLength]; //last stored value
	for (int i=0; i < smoothWidth; ++i)
	{
		buffer[c++ %bufferLength] = val;

		//average value of the buffer is the output
		newVal = 0;
		for (int i=0; i < bufferLength; ++i) newVal += buffer[i];
		newVal /= (V)bufferLength;
		std::cout << newVal << "\n";
	}
}


int main(int argc,char** argv)
{
	//read numbers and smooth them by taking average of 5 (2 numbers around)
	if (argc == 1)
	{
		std::cout << "provide one argument w to have averaging window of size 2*w+1\n";
		std::cout << "treats values as integers!\n";
		return 1;
	}

	int w = atol(argv[1]);
	process<int>(w);

	return 0;
}
