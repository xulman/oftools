/*
 * Tools related to the MitoPacq, the CBIA time-lapse cell observation simulator.
 *
 * Copyright (C) 2005-2013   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library-related tools is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <iostream>
#include <i3d/image3d.h>


template <class VT>
void upscale(const i3d::Image3d<VT>& srcImg, i3d::Image3d<VT>& tgtImg, const size_t xCopies, const size_t yCopies)
{
	std::cout << "upscaling : " << xCopies << " X " << yCopies << " times per axis\n";

	tgtImg.CopyMetaData(srcImg);
	tgtImg.MakeRoom(xCopies*srcImg.GetSizeX(), yCopies*srcImg.GetSizeY(), srcImg.GetSizeZ());

	const size_t lineBytes = sizeof(VT)*srcImg.GetSizeX();
	VT* tgtP = tgtImg.GetFirstVoxelAddr();

	for (size_t z=0; z < srcImg.GetSizeZ(); ++z)
	{
		for (size_t my=0; my < yCopies; ++my)
		{
			const VT* srcP = srcImg.GetVoxelAddr(0,0,z);
			for (size_t y=0; y < srcImg.GetSizeY(); ++y)
			{
				for (size_t mx=0; mx < xCopies; ++mx)
				{
					memcpy(tgtP,srcP,lineBytes);
					tgtP += srcImg.GetSizeX();
				}
				srcP += srcImg.GetSizeX();
			}
		}
	}
}


int main(int argc,char** argv)
{
	std::cout << argv[0] << " - part of the tools set of the OpticalFlow library \n"
		"Copyright (C) 2005-2013 Centre for Biomedical Image Analysis (CBIA)\n"
		"This is free software; see the source for copying conditions. "
		"There is NO\nwarranty; not even for MERCHANTABILITY or FITNESS FOR "
		"A PARTICULAR PURPOSE.\n\n";

	if (argc < 5 || (argc-2)%3 != 0)
	{
		std::cout << "Program stacks the given image as tiles next to each other into a given grid(s).\n";
		std::cout << "Bad number of parameters\n";
		std::cout << "Usage: " << argv[0] << " tileImage.tif out1.tif x1 y1 ... outN.tif xN yN\n";
		return -1;
	}

	std::cout << "reading   : " << argv[1] << "\n";
	const i3d::Image3d<i3d::GRAY16> sourceImg(argv[1]);
	i3d::Image3d<i3d::GRAY16> targetImg;

	int currArgPos = 2;
	while (currArgPos < argc)
	{
		//readout output filename and grid shape
		int x = std::atoi(argv[currArgPos+1]);
		int y = std::atoi(argv[currArgPos+2]);

		std::cout << "processing: " << argv[currArgPos] << "\n";
		upscale(sourceImg,targetImg,x,y);
		std::cout << "saving    : " << argv[currArgPos] << "\n";
		targetImg.SaveImage(argv[currArgPos]);
		std::cout << "done\n";

		currArgPos += 3;
	}

	return 0;
}
