/*
 * Tools related to the MitoPacq, the CBIA time-lapse cell observation simulator.
 *
 * Copyright (C) 2005-2013   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library-related tools is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <i3d/image3d.h>

using namespace std;
using namespace i3d;


int main(int argc, char **argv)
{
    cout << argv[0] << " - part of the tools set of the OpticalFlow library \n"
         "Copyright (C) 2005-2013 Centre for Biomedical Image Analysis (CBIA)\n"
         "This is free software; see the source for copying conditions. "
         "There is NO\nwarranty; not even for MERCHANTABILITY or FITNESS FOR "
         "A PARTICULAR PURPOSE.\n\n";

    if ((argc != 9) && (argc != 10))
    {
        cout << "Program crops input image and stores the result in output image.\n";
	cout << "The VOI's corner is at (x,y,z) and the size is (x_size,y_size,z_size).\n";
	cout << "The [xyz]_size parameters must be strictly positive. The x,y,z offsets\n";
	cout << "may be negative, in which case the program extends the input image by (-x,-y,-z).\n";
	cout << "Similarily, if any (or all) of the [xyz]_size parameters would render the corner of\n";
	cout << "the VOI outside the input image, the image will be extended appropriately.\n";
	cout << "By default, the offset of the output image is set adequately. If the last\n";
	cout << "option is supplied, the offset is set to (0,0,0) instead [zero offset].\n\n";

        cout << "Bad number of parameters\n" << "Usage: " << argv[0] << " input output x y z x_size y_size z_size [zo]\n";
        return -2;
    }

	 const bool zo= ((argc == 10) && (argv[9][0] == 'z') && (argv[9][1] == 'o'))? true : false;

    try
    {
        cerr << "Reading image Type" << endl;
        ImgVoxelType vt = ReadImageType(argv[1]);
        cerr << "Voxel=" << VoxelTypeToString(vt) << endl;

        struct VOI <PIXELS > voi((int) atoi(argv[3]), (int) atoi(argv[4]), (int) atoi(argv[5]), (size_t) atoi(argv[6]), (size_t) atoi(argv[7]), (size_t) atoi(argv[8]));

        switch (vt)
        {
        case Gray8Voxel:
            {
		Image3d <GRAY8> Input(argv[1]);
		Image3d <GRAY8> Output;
		Output.CopyFromVOI(Input, voi, true);
		if (zo) Output.SetOffset(i3d::Offset(0.,0.,0.));
		Output.SaveImage(argv[2]);
		break;
            }
         case Gray16Voxel:
            {
		Image3d <GRAY16> Input(argv[1]);
		Image3d <GRAY16> Output;
		Output.CopyFromVOI(Input, voi, true);
		if (zo) Output.SetOffset(i3d::Offset(0.,0.,0.));
		Output.SaveImage(argv[2]);
		break;
            }
         case RGBVoxel:
            {
		Image3d <RGB> Input(argv[1]);
		Image3d <RGB> Output;
		Output.CopyFromVOI(Input, voi, true);
		if (zo) Output.SetOffset(i3d::Offset(0.,0.,0.));
		Output.SaveImage(argv[2]);
		break;
            }
         case RGB16Voxel:
            {
		Image3d <RGB16> Input(argv[1]);
		Image3d <RGB16> Output;
		Output.CopyFromVOI(Input, voi, true);
		if (zo) Output.SetOffset(i3d::Offset(0.,0.,0.));
		Output.SaveImage(argv[2]);
		break;
            }
        case FloatVoxel:
            {
		Image3d <float> Input(argv[1]);
		Image3d <float> Output;
		Output.CopyFromVOI(Input, voi, true);
		if (zo) Output.SetOffset(i3d::Offset(0.,0.,0.));
		Output.SaveImage(argv[2]);
		break;
            }
        default:
            cerr << "Other voxel types not supported" << endl;
        }
    }
    catch(IOException & e)
    {
        cerr << e << endl;
        exit(1);
    }
    catch(InternalException & e)
    {
        cerr << e << endl;
        exit(1);
    }
    catch(bad_alloc &)
    {
        cerr << "Not enough memory." << endl;
        exit(1);
    }
    catch(...)
    {
        cerr << argv[0] << "Unknown exception." << endl;
        exit(1);
    }
    return 0;
}
