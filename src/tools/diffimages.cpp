/*
 * Tools related to the MitoPacq, the CBIA time-lapse cell observation simulator.
 *
 * Copyright (C) 2005-2013   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library-related tools is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <i3d/image3d.h>
#include <i3d/basic.h>

using namespace i3d;
using namespace std;


template<typename VT>
void diff(char** argv)
{
	i3d::Image3d<VT> a(argv[1]);
	i3d::Image3d<VT> b(argv[2]);

	if (a.GetSize() != b.GetSize())
	{
		cout << "Images are of different size!\n";
		return;
	}

	const VT* pa = a.GetFirstVoxelAddr();
	const VT* pb = b.GetFirstVoxelAddr();
	const VT* const pbL = pb + b.GetImageSize();
	size_t off = 0;
	for (; pb != pbL; ++pa, ++pb, ++off)
	{
		if (*pb != *pa)
			std::cout << a.GetPos(off) << ": L " << *pa << " vs. R " << *pb << "\n";
	}
}


int main(int argc, char **argv)
{
	cout << argv[0] << " - part of the tools set of the OpticalFlow library \n"
		"Copyright (C) 2005-2013 Centre for Biomedical Image Analysis (CBIA)\n"
		"This is free software; see the source for copying conditions. "
		"There is NO\nwarranty; not even for MERCHANTABILITY or FITNESS FOR "
		"A PARTICULAR PURPOSE.\n\n";

	if (argc != 3)
	{
		cout << "params: firstImage secondImage\n";
		cout << "it reports difference between the two; the images must be of the same size\n";
		return -2;
	}

	// zjisteni typu vstupniho souboru
	// !!! Nefunguje zjisteni souboru z regularniho vyrazu !!!
	cerr << "Reading image Type" << endl;
	ImgVoxelType vt = ReadImageType(argv[1]);
	cerr << "Voxel=" << VoxelTypeToString(vt) << endl;

	switch (vt)
	{
	case Gray8Voxel:
		diff<i3d::GRAY8>(argv);
		break;
	case Gray16Voxel:
		diff<i3d::GRAY16>(argv);
		break;
	default:
		cerr << "Other voxel types than G8 or G16 are not supported!\n";
		break;
	}

	return 0;
}
