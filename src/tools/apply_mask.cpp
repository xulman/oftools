/*
 * Tools related to the MitoPacq, the CBIA time-lapse cell observation simulator.
 *
 * Copyright (C) 2005-2013   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library-related tools is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <iostream>
#include <i3d/image3d.h>


using std::cout;
using std::endl;

template <class MT,class VT>
void ApplyMask(i3d::Image3d<MT> const &mask, i3d::Image3d<VT> &pha) {
	const MT *pm=mask.GetFirstVoxelAddr();
	const MT *pmL=mask.GetFirstVoxelAddr() + mask.GetImageSize();
	VT *pp=pha.GetFirstVoxelAddr();

	while (pm != pmL) {
		if (*pm == 0) *pp=0;
		++pm; ++pp;
	}
}

int main(int argc, char **argv) {
    cout << argv[0] << " - part of the tools set of the OpticalFlow library \n"
         "Copyright (C) 2005-2013 Centre for Biomedical Image Analysis (CBIA)\n"
         "This is free software; see the source for copying conditions. "
         "There is NO\nwarranty; not even for MERCHANTABILITY or FITNESS FOR "
         "A PARTICULAR PURPOSE.\n\n";

	//type of mask images
	typedef i3d::GRAY16 MV;
	typedef i3d::GRAY16 PV;

		//read inputs, mask first then phantom
		cout << "processing mask " << argv[1] << " and phantom " << argv[2] << endl;
		i3d::Image3d<MV> mask(argv[1]);
		i3d::Image3d<PV> phantom(argv[2]);

		ApplyMask(mask,phantom);

		//write polished phantom
		phantom.SaveImage(argv[2]);

	return(0);
}
