/*
 * Tools related to the MitoPacq, the CBIA time-lapse cell observation simulator.
 *
 * Copyright (C) 2005-2013   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library-related tools is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <i3d/image3d.h>
#include <i3d/basic.h>
#include <fstream>

using namespace i3d;
using namespace std;

template <class VOXELIN> void DoGNUplotImage  (Image3d<VOXELIN> & inImg, char * name)
{
    fstream filestr;
    const size_t xIndMax = inImg.GetSizeX();
    const size_t yIndMax = inImg.GetSizeY();
    const size_t zIndMax = inImg.GetSizeZ();

    filestr.open(name, std::ios::out);

    if (zIndMax == 1)
    {
      // 2D case, long column of voxel positions with their values 
      cout << "Converting 2D image " << xIndMax << " x " << yIndMax << " ...\n";

      for (size_t x = 0; x < xIndMax; ++x)
	for (size_t y = 0; y < yIndMax; ++y)
	    filestr << x << " " << y << " " << static_cast<float>(inImg.GetVoxel(x,y,0)) << endl;
    }
    else
    {
      // 3D case, each slice is separated by empty line,
      // each slice is printed in a matrix layout
      cout << "Converting 3D image " << xIndMax << " x " << yIndMax << " x " << zIndMax << " ...\n";

      for (size_t x = 0; x < xIndMax; ++x)
       {

       for (size_t y = 0; y < yIndMax; ++y) //exporting of slice
         for (size_t z = 0; z < zIndMax; ++z)
	    filestr << x << " " << y << " " << z << " " << static_cast<float>(inImg.GetVoxel(x,y,z)) << endl;

       filestr << endl;
       }
    }

    filestr.close();
}


int main(int argc, char **argv)
{
    cout << argv[0] << " - part of the tools set of the OpticalFlow library \n"
         "Copyright (C) 2005-2013 Centre for Biomedical Image Analysis (CBIA)\n"
         "This is free software; see the source for copying conditions. "
         "There is NO\nwarranty; not even for MERCHANTABILITY or FITNESS FOR "
         "A PARTICULAR PURPOSE.\n\n";


    Image3d <i3d::GRAY8> g8Img;
    Image3d <i3d::GRAY16> g16Img;
    Image3d <float> fImg;

    if (argc < 3)
    {
        cout << "Bad number of parameters\nUsage: " << argv[0] << " input output\nexample: " << argv[0] << " image.tif image.bin\n" ;
        return -2;
    }
        // zjisteni typu vstupniho souboru
        // !!! Nefunguje zjisteni souboru z regularniho vyrazu !!!
        cerr << "Reading image Type" << endl;
        ImgVoxelType vt = ReadImageType(argv[1]);
        cerr << "Voxel=" << VoxelTypeToString(vt) << endl;

    try
    {
	switch (vt)
	{
	    case Gray8Voxel:
		g8Img.ReadImage(argv[1]);
		DoGNUplotImage(g8Img, argv[2]);
		break;
	    case Gray16Voxel:
		g16Img.ReadImage(argv[1]);
		DoGNUplotImage(g16Img, argv[2]);
		break;
	    case FloatVoxel:
		fImg.ReadImage(argv[1]);
		DoGNUplotImage(fImg, argv[2]);

	    default:
		cerr << "Other voxel types not supported!\n";
		break;

	}
    }
    catch(IOException & e)
    {
        cerr << e << endl;
        exit(1);
    }
    catch(InternalException & e)
    {
        cerr << e << endl;
        exit(1);
    }
    catch(bad_alloc &)
    {
        cerr << "Not enough memory." << endl;
        exit(1);
    }
    catch(...)
    {
        cerr << argv[0] << "Unknown exception." << endl;
        exit(1);
    }
    return 0;
}
