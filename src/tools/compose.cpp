/*
 * Tools related to the MitoPacq, the CBIA time-lapse cell observation simulator.
 *
 * Copyright (C) 2005-2013   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library-related tools is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <i3d/image3d.h>
#include <i3d/basic.h>
#include <iostream>

template <class VOXEL, class VOXELcomposed> void DoCompose(char *x_name, char *y_name, char *z_name, char *xyz_name)
{
    i3d::Image3d <VOXEL> XImg(x_name);
    i3d::Image3d <VOXEL> YImg(y_name);
    i3d::Image3d <VOXEL> ZImg(z_name);
    i3d::Image3d <VOXELcomposed> XYZImg;

    XYZImg.CopyMetaData(XImg);

    for (size_t i = 0; i < XYZImg.GetImageSize(); ++i)
	XYZImg.SetVoxel(i, VOXELcomposed(XImg.GetVoxel(i),YImg.GetVoxel(i),ZImg.GetVoxel(i)) );

    XYZImg.SaveImage(xyz_name);
}

template <class VOXEL, class VOXELcomposed> void DoComposeB(char *x_name, char *y_name, char *xyz_name)
{
    i3d::Image3d <VOXEL> XImg(x_name);
    i3d::Image3d <VOXEL> YImg(y_name);
    i3d::Image3d <VOXELcomposed> XYZImg;

    XYZImg.CopyMetaData(XImg);

    for (size_t i = 0; i < XYZImg.GetImageSize(); ++i)
	XYZImg.SetVoxel(i, VOXELcomposed(XImg.GetVoxel(i),YImg.GetVoxel(i),0) );

    XYZImg.SaveImage(xyz_name);
}

template <class VOXEL> void DoCompose(char *x_name, char *y_name, char *z_name, char *xyz_name, const VOXEL mult)
{
    i3d::Image3d <VOXEL> XImg(x_name);
    i3d::Image3d <VOXEL> YImg(y_name);
    i3d::Image3d <VOXEL> ZImg(z_name);
    i3d::Image3d <i3d::RGB16> XYZImg;

    XYZImg.CopyMetaData(XImg);

    for (size_t i = 0; i < XYZImg.GetImageSize(); ++i)
	XYZImg.SetVoxel(i, i3d::RGB16(static_cast<i3d::GRAY16>(mult*XImg.GetVoxel(i)), \
				static_cast<i3d::GRAY16>(mult*YImg.GetVoxel(i)), \
				static_cast<i3d::GRAY16>(mult*ZImg.GetVoxel(i))) );

    XYZImg.SaveImage(xyz_name);
}

template <class VOXEL> void DoComposeB(char *x_name, char *y_name, char *xyz_name, const VOXEL mult)
{
    i3d::Image3d <VOXEL> XImg(x_name);
    i3d::Image3d <VOXEL> YImg(y_name);
    i3d::Image3d <i3d::RGB16> XYZImg;

    XYZImg.CopyMetaData(XImg);

    for (size_t i = 0; i < XYZImg.GetImageSize(); ++i)
	XYZImg.SetVoxel(i, i3d::RGB16(static_cast<i3d::GRAY16>(mult*XImg.GetVoxel(i)), \
				static_cast<i3d::GRAY16>(mult*YImg.GetVoxel(i)), \
				0) );

    XYZImg.SaveImage(xyz_name);
}


int main(int argc, char **argv)
{
    std::cout << argv[0] << " - part of the tools set of the OpticalFlow library \n"
         "Copyright (C) 2005-2013 Centre for Biomedical Image Analysis (CBIA)\n"
         "This is free software; see the source for copying conditions. "
         "There is NO\nwarranty; not even for MERCHANTABILITY or FITNESS FOR "
         "A PARTICULAR PURPOSE.\n\n";


    if ((argc != 4) && (argc != 5) && (argc != 6))
    {
        std::cout << "Bad number of parameters\n";
	std::cout << "Usage: " << argv[0] << " r_input g_input b_input rgb_output [_number]\n";
	std::cout << " OR  : " << argv[0] << " r_input g_input rg_output [_number]\n\n";
	std::cout << "example: " << argv[0] << " r.ics g.ics b.ics rgb.ics _1000\n";
	std::cout << "the last optional parameter _1000 is only used when float images are to be composed,\n";
	std::cout << "float images are voxel-wise multiplied with 1000 and are then converted to RGB16\n";
	std::cout << "(this is to \"normalize\" floats before being converted to \"large\" grays)\n";
        return -2;
    }
    i3d::ImgVoxelType vt = i3d::ReadImageType(argv[1]);
    std::cerr << "Voxel=" << VoxelTypeToString(vt) << std::endl;

    if ((argc >= 5) && (argv[4][0] != '_')) {
    switch (vt)
    {
    case i3d::Gray8Voxel:
        {
            DoCompose <i3d::GRAY8,i3d::RGB> (argv[1], argv[2], argv[3], argv[4]);
            break;
        }
    case i3d::Gray16Voxel:
        {
            DoCompose <i3d::GRAY16,i3d::RGB16> (argv[1], argv[2], argv[3], argv[4]);
            break;
        }
    case i3d::FloatVoxel:
        {
	    float mult=1.0f;
	    if (argc == 6) mult=atof(argv[5]+1);
	    std::cout << "will convert into RGB16 and multiply by " << mult << " beforehand\n";
            DoCompose <float> (argv[1], argv[2], argv[3], argv[4], mult);
	    break;
        }
    case i3d::DoubleVoxel:
        {
	    double mult=1.0f;
	    if (argc == 6) mult=atof(argv[5]+1);
	    std::cout << "will convert into RGB16 and multiply by " << mult << " beforehand\n";
            DoCompose <double> (argv[1], argv[2], argv[3], argv[4], mult);
	    break;
        }
    default:
        std::cerr << "Other voxel types not supported" << std::endl;
    }
    } else {
    switch (vt)
    {
    case i3d::Gray8Voxel:
        {
            DoComposeB <i3d::GRAY8,i3d::RGB> (argv[1], argv[2], argv[3]);
            break;
        }
    case i3d::Gray16Voxel:
        {
            DoComposeB <i3d::GRAY16,i3d::RGB16> (argv[1], argv[2], argv[3]);
            break;
        }
    case i3d::FloatVoxel:
        {
	    float mult=1.0f;
	    if (argc == 5) mult=atof(argv[4]+1);
	    std::cout << "will convert into RGB16 and multiply by " << mult << " beforehand\n";
            DoComposeB <float> (argv[1], argv[2], argv[3], mult);
	    break;
        }
    case i3d::DoubleVoxel:
        {
	    double mult=1.0;
	    if (argc == 5) mult=atof(argv[4]+1);
	    std::cout << "will convert into RGB16 and multiply by " << mult << " beforehand\n";
            DoComposeB <double> (argv[1], argv[2], argv[3], mult);
	    break;
        }
    default:
        std::cerr << "Other voxel types not supported" << std::endl;
    }
    }
    return 0;
}
