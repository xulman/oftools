/*
 * Tools related to the MitoPacq, the CBIA time-lapse cell observation simulator.
 *
 * Copyright (C) 2005-2013   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library-related tools is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <i3d/image3d.h>
#include <i3d/DistanceTransform.h>
#include <i3d/basic.h>

using namespace i3d;
using namespace std;


int main(int argc, char **argv)
{
    cout << argv[0] << " - part of the tools set of the OpticalFlow library \n"
         "Copyright (C) 2005-2013 Centre for Biomedical Image Analysis (CBIA)\n"
         "This is free software; see the source for copying conditions. "
         "There is NO\nwarranty; not even for MERCHANTABILITY or FITNESS FOR "
         "A PARTICULAR PURPOSE.\n\n";


    Image3d <i3d::GRAY8> g8Img;
    Image3d <float> fImg;
    Image3d <float> dtImg;

    if (argc < 3)
    {
        cout << "Bad number of parameters\nUsage: " << argv[0] << " input output [n]\nexample: " << argv[0] << " input.tif dt.mhd\nor\n" << argv[0] << " mask.tif distancetransform.mhd \n" <<
            "T\n";
        return -2;
    }
        cerr << "Reading image Type" << endl;
        ImgVoxelType vt = ReadImageType(argv[1]);
        cerr << "Voxel=" << VoxelTypeToString(vt) << endl;

    try
    {
	switch (vt)
	{
	    case Gray8Voxel:
		g8Img.ReadImage(argv[1]);
		g8Img.SetResolution(Resolution(1,1,1));
		fImg.SetResolution(Resolution(1,1,1));
		GrayToFloat(g8Img,fImg);

//		 EDM(fImg, 1,255.0f, true);
		 FastSaito(fImg,0.0f,true);

		 FloatToGray(fImg, g8Img);

		 for (size_t y = 0 ; y < fImg.GetSizeY(); ++y)
		 for (size_t x = 0 ; x < fImg.GetSizeX(); ++x)
		     if ((x > 19 && x < 298)&&(y>34&&y<229))
			 fImg.SetVoxel(x,y,0,fImg.GetVoxel(x,y,0)*-1.0f);

		 fImg.GetVoxelData() += 100;
		 FloatToGrayNoWeight(fImg, g8Img);

		 g8Img.SaveImage("dtg8.tif");
		 fImg.SaveImage(argv[2]);
		break;
	    default:
		cerr << "Other voxel types not supported!\n";
		break;

	}
    }
    catch(IOException & e)
    {
        cerr << e << endl;
        exit(1);
    }
    catch(InternalException & e)
    {
        cerr << e << endl;
        exit(1);
    }
    catch(bad_alloc &)
    {
        cerr << "Not enough memory." << endl;
        exit(1);
    }
    catch(...)
    {
        cerr << argv[0] << "Unknown exception." << endl;
        exit(1);
    }


    return 0;
}
