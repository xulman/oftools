/*
 * Tools related to the MitoPacq, the CBIA time-lapse cell observation simulator.
 *
 * Copyright (C) 2005-2013   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library-related tools is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <i3d/image3d.h>
#include <i3d/basic.h>
#include <i3d/histogram.h>
#include <iostream>
#ifdef _MSC_VER
#include <time.h>
#else
#include <sys/time.h>
#endif
#include <ctype.h>

template <class VOXEL> void PrintRange(i3d::Image3d<VOXEL> & inputImage)
{
    VOXEL minRange, maxRange;
    inputImage.GetRange(minRange, maxRange);
    std::cout << "Image range: [ " << static_cast<double>(minRange) << ", " << static_cast<double>(maxRange) << " ]\n";

	 i3d::Histogram h;
	 i3d::IntensityHist(inputImage,h);
	 int count=0;
	 for (size_t i=0; i < h.size(); ++i)
	 	count+=(h[i] > 0)? 1 : 0;

	 std::cout << "There is " << count << " intensities used in the image.\n";
}

int main(int argc, char **argv)
{
    std::cout << argv[0] << " - part of the tools set of the OpticalFlow library \n"
         "Copyright (C) 2005-2013 Centre for Biomedical Image Analysis (CBIA)\n"
         "This is free software; see the source for copying conditions. "
         "There is NO\nwarranty; not even for MERCHANTABILITY or FITNESS FOR "
         "A PARTICULAR PURPOSE.\n\n";


    if (argc < 2)
    {
        std::cout << "Computes the number of intensities in the histogram (e.g. number of components\n";
		  std::cout << "in a labelled mask) as well as minimal and maximal intensity value.\n\n";
        std::cout << "Bad number of parameters\nUsage: " << argv[0] << " input\n";
		  std::cout << "example: " << argv[0] << " image.tif\n";
        return -2;
    }
    i3d::ImgVoxelType vt = i3d::ReadImageType(argv[1]);
    std::cerr << "Voxel=" << VoxelTypeToString(vt) << std::endl;

    switch (vt)
    {
    case i3d::FloatVoxel:
        {
            i3d::Image3d <float>Img(argv[1]);
	    PrintRange(Img);
        }
        break;
    case i3d::Gray8Voxel:
        {
            i3d::Image3d <i3d::GRAY8>Img(argv[1]);
	    PrintRange(Img);
        }
        break;
 
    case i3d::Gray16Voxel:
        {
            i3d::Image3d <i3d::GRAY16>Img(argv[1]);
	    PrintRange(Img);
        }
        break;
    default:
        std::cerr << "Other voxel types not supported" << std::endl;
    }
    return 0;
}
