/*
 * Tools related to the MitoPacq, the CBIA time-lapse cell observation simulator.
 *
 * Copyright (C) 2005-2013   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library-related tools is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <i3d/image3d.h>
#include <i3d/basic.h>
#include <iostream>
#ifdef _MSC_VER
#include <time.h>
#else
#include <sys/time.h>
#endif
#include <ctype.h>

template <class VOXEL> void DoTransposeXZtoXY(char *file_name, char *out_file_name)
{
    i3d::Image3d <VOXEL> In(file_name);
    i3d::Image3d <VOXEL> Out;
    Out.MakeRoom(In.GetSizeX(),In.GetSizeZ(),In.GetSizeY());
    Out.SetResolution(In.GetResolution());
    Out.SetOffset(In.GetOffset());

    for (unsigned int z=0; z < In.GetSizeZ(); ++z)
      for (unsigned int y=0; y < In.GetSizeY(); ++y)
        for (unsigned int x=0; x < In.GetSizeX(); ++x)
		Out.SetVoxel(x,z,y,In.GetVoxel(x,y,z));

    Out.SaveImage(out_file_name);
}

template <class VOXEL> void DoTransposeYZtoXY(char *file_name, char *out_file_name)
{
    i3d::Image3d <VOXEL> In(file_name);
    i3d::Image3d <VOXEL> Out;
    Out.MakeRoom(In.GetSizeZ(),In.GetSizeY(),In.GetSizeX());
    Out.SetResolution(In.GetResolution());
    Out.SetOffset(In.GetOffset());

    for (unsigned int z=0; z < In.GetSizeZ(); ++z)
      for (unsigned int y=0; y < In.GetSizeY(); ++y)
        for (unsigned int x=0; x < In.GetSizeX(); ++x)
		Out.SetVoxel(z,y,x,In.GetVoxel(x,y,z));

    Out.SaveImage(out_file_name);
}

int main(int argc, char **argv)
{
    std::cout << argv[0] << " - part of the tools set of the OpticalFlow library \n"
         "Copyright (C) 2005-2013 Centre for Biomedical Image Analysis (CBIA)\n"
         "This is free software; see the source for copying conditions. "
         "There is NO\nwarranty; not even for MERCHANTABILITY or FITNESS FOR "
         "A PARTICULAR PURPOSE.\n\n";

    int what_to_do=0; //0 - error, 1 - XZ, 2 - YZ
    if (argc >= 4) {
	    if ((argv[3][0]=='x') && (argv[3][1]=='z')) what_to_do=1;
	    if ((argv[3][0]=='y') && (argv[3][1]=='z')) what_to_do=2;
    }

    if ((argc != 4) || (what_to_do == 0))
    {
	std::cout << "Program transposes input 3D image and stores result in output image.\n";
	std::cout << "In fact, the XZ planes, which are normally stacked along the y axis in the input image,\n";
	std::cout << "will become XY planes and will be stacked along the z axis in the output image.\n";
	std::cout << "In the same fashion, the input YZ planes will become output XY planes. In both cases,\n";
	std::cout << "the size of output image will be appropriatelly adjusted.\n\n";

        std::cout << "Bad number of parameters\nUsage: " << argv[0] << " input output [xz|yz]\nexample: " << argv[0] << "original.tif flipped.tif xz\n";
        return -2;
    }
    i3d::ImgVoxelType vt = i3d::ReadImageType(argv[1]);
    std::cerr << "Voxel=" << VoxelTypeToString(vt) << std::endl;

    switch (vt)
    {
    case i3d::Gray8Voxel:
        {
            if (what_to_do == 1) DoTransposeXZtoXY <i3d::GRAY8> (argv[1], argv[2]);
	    else		 DoTransposeYZtoXY <i3d::GRAY8> (argv[1], argv[2]);
            break;
        }
    case i3d::Gray16Voxel:
        {
            if (what_to_do == 1) DoTransposeXZtoXY <i3d::GRAY16> (argv[1], argv[2]);
	    else 		 DoTransposeYZtoXY <i3d::GRAY16> (argv[1], argv[2]);
            break;
        }
    case i3d::RGBVoxel:
	{
            if (what_to_do == 1) DoTransposeXZtoXY <i3d::RGB> (argv[1], argv[2]);
	    else 		 DoTransposeYZtoXY <i3d::RGB> (argv[1], argv[2]);
            break;
	}
    case i3d::RGB16Voxel:
	{
            if (what_to_do == 1) DoTransposeXZtoXY <i3d::RGB16> (argv[1], argv[2]);
	    else 		 DoTransposeYZtoXY <i3d::RGB16> (argv[1], argv[2]);
            break;
	}
    case i3d::FloatVoxel:
        {
            if (what_to_do == 1) DoTransposeXZtoXY <float>(argv[1], argv[2]);
	    else 		 DoTransposeYZtoXY <float>(argv[1], argv[2]);
	    break;
        }
    default:
        std::cerr << "Other voxel types not supported" << std::endl;
    }
    return 0;
}
