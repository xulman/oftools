/*
 * Tools related to the MitoPacq, the CBIA time-lapse cell observation simulator.
 *
 * Copyright (C) 2005-2013   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library-related tools is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <i3d/image3d.h>
#include <i3d/basic.h>

using namespace i3d;
using namespace std;

int main(int argc, char **argv)
{
    cout << argv[0] << " - part of the tools set of the OpticalFlow library \n"
         "Copyright (C) 2005-2013 Centre for Biomedical Image Analysis (CBIA)\n"
         "This is free software; see the source for copying conditions. "
         "There is NO\nwarranty; not even for MERCHANTABILITY or FITNESS FOR "
         "A PARTICULAR PURPOSE.\n\n";


    Image3d <i3d::GRAY8> Img;
    Image3d <float> FImg;

    if (argc < 3)
    {
        cout << "Bad number of parameters\nUsage: " << argv[0] << " input output [n]\nexample: " << argv[0] << " floatimage.mhd grayimage.tif\nor\n" << argv[0] << " floatimage.mhd grayimage.tif n\n" <<
            "The third parameter is whether to stretch (without parameter) or not to stretch (with parameter)\n";
        return -2;
    }
    try
    {
        FImg.ReadImage(argv[1]);
        if (argc == 3)
            FloatToGray(FImg, Img);
        else
            FloatToGrayNoWeight(FImg, Img);
        Img.SaveImage(argv[2]);
    }
    catch(IOException & e)
    {
        cerr << e << endl;
        exit(1);
    }
    catch(InternalException & e)
    {
        cerr << e << endl;
        exit(1);
    }
    catch(bad_alloc &)
    {
        cerr << "Not enough memory." << endl;
        exit(1);
    }
    catch(...)
    {
        cerr << argv[0] << "Unknown exception." << endl;
        exit(1);
    }


    return 0;
}
