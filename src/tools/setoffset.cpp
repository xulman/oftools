/*
 * Tools related to the MitoPacq, the CBIA time-lapse cell observation simulator.
 *
 * Copyright (C) 2005-2013   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library-related tools is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <iostream>
#include <i3d/image3d.h>
#include <i3d/imgfiles.h>
using namespace std;
using namespace i3d;

int main(int argc, char *argv[])
{
    cout << argv[0] << " - part of the tools set of the OpticalFlow library \n"
         "Copyright (C) 2005-2013 Centre for Biomedical Image Analysis (CBIA)\n"
         "This is free software; see the source for copying conditions. "
         "There is NO\nwarranty; not even for MERCHANTABILITY or FITNESS FOR "
         "A PARTICULAR PURPOSE.\n\n";

    if (argc < 6)
    {
        cout << "Bad number of parameters\nUsage: " << argv[0] << " input output res_x res_y res_z\nexample: " << argv[0] << " input.tif output.tif 1 1 1\n";
	cout << "This program serves for offset (position) setting of the image.\n";
        exit(1);
    }

    try
    {
        // zjisteni typu vstupniho souboru
        // !!! Nefunguje zjisteni souboru z regularniho vyrazu !!!
        cerr << "Reading image Type" << endl;
        ImgVoxelType vt = ReadImageType(argv[1]);
        cerr << "Voxel=" << VoxelTypeToString(vt) << endl;
	float resX, resY, resZ;

	resX = atof(argv[3]);
	resY = atof(argv[4]);
	resZ = atof(argv[5]);
	Offset off(resX, resY, resZ);
        switch (vt)
        {
        case Gray8Voxel:
            {
                Image3d <GRAY8> i(argv[1]);
		i.SetOffset(off);
                i.SaveImage(argv[2]);
                break;
            }
        case Gray16Voxel:
            {
                Image3d <GRAY16> i(argv[1]);
		i.SetOffset(off);
                i.SaveImage(argv[2]);
                break;
            }
        case FloatVoxel:
            {
                Image3d <float>i(argv[1]);
		i.SetOffset(off);
                i.SaveImage(argv[2]);
                break;
            }
        case RGBVoxel:
            {
                Image3d <RGB>i(argv[1]);
		i.SetOffset(off);
                i.SaveImage(argv[2]);
                break;
            }
        case RGB16Voxel:
            {
                Image3d <RGB16>i(argv[1]);
		i.SetOffset(off);
                i.SaveImage(argv[2]);
                break;
            }

        default:
            cerr << "Other voxel types not supported" << endl;
        }
    }
    catch(IOException & e)
    {
        cerr << e << endl;
        exit(1);
    }
    catch(InternalException & e)
    {
        cerr << e << endl;
        exit(1);
    }
    catch(bad_alloc &)
    {
        cerr << "Not enough memory." << endl;
        exit(1);
    }
    catch(...)
    {
        cerr << argv[0] << "Unknown exception." << endl;
        exit(1);
    }

    return 0;
}
