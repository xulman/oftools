/*
 * Tools related to the MitoPacq, the CBIA time-lapse cell observation simulator.
 *
 * Copyright (C) 2005-2013   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library-related tools is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <iostream>
#include <i3d/image3d.h>
#include <i3d/imgfiles.h>
#include <i3d/toolbox.h>
using namespace std;
using namespace i3d;

template <class VOXEL> void GetImageInfo(Image3d<VOXEL> & image)
{
    cout << "Image size px: " << image.GetSize() << " px\n";
    cout << "Image size um: " << i3d::PixelsToMicrons(image.GetSize(),image.GetResolution()) << " um\n";
    cout << "Image resolution: " << image.GetResolution().GetRes() << " px/um\n";
    cout << "Image offset: " << image.GetOffset() << " um\n";
	 size_t ImageSize=image.GetImageSize()*sizeof(VOXEL);
	 cout << "Image size: " << ImageSize << " B\n";
	 if (ImageSize > (size_t(1) << 30)) cout << "Image size human: " << (ImageSize >> 30) << " GB\n";
	 else
	 if (ImageSize > (size_t(1) << 20)) cout << "Image size human: " << (ImageSize >> 20) << " MB\n";
	 else
	 if (ImageSize > (size_t(1) << 10)) cout << "Image size human: " << (ImageSize >> 10) << " kB\n";
	 else
	 cout << "Image size human: " << ImageSize << " B\n";
	 cout << "Current free memory can host " << i3d::GetFreeMemory()/ImageSize << " copies of this image.\n";
}

int main(int argc, char *argv[])
{
    cout << argv[0] << " - part of the tools set of the OpticalFlow library \n"
         "Copyright (C) 2005-2013 Centre for Biomedical Image Analysis (CBIA)\n"
         "This is free software; see the source for copying conditions. "
         "There is NO\nwarranty; not even for MERCHANTABILITY or FITNESS FOR "
         "A PARTICULAR PURPOSE.\n\n";

    if (argc != 2)
    {
        cout << "Bad number of parameters\nUsage: " << argv[0] << " input\nexample: " << argv[0] << " image.ics\n";
	cout << "This program returns info about the image.\n";
        exit(1);
    }

    try
    {
        // zjisteni typu vstupniho souboru
        // !!! Nefunguje zjisteni souboru z regularniho vyrazu !!!
        ImgVoxelType vt = ReadImageType(argv[1]);
        cout << "Image voxel type=" << VoxelTypeToString(vt) << endl;

        switch (vt)
        {
        case BinaryVoxel:
            {
                Image3d <bool> i(argv[1]);
		GetImageInfo(i);
                break;
            }

        case Gray8Voxel:
            {
                Image3d <GRAY8> i(argv[1]);
		GetImageInfo(i);
                break;
            }

        case Gray16Voxel:
            {
                Image3d <GRAY16> i(argv[1]);
		GetImageInfo(i);
                break;
            }

        case FloatVoxel:
            {
                Image3d <float>i(argv[1]);
		GetImageInfo(i);
                break;
            }

        case RGBVoxel:
            {
                Image3d <RGB>i(argv[1]);
		GetImageInfo(i);
                break;
            }

         case RGB16Voxel:
            {
                Image3d <RGB16>i(argv[1]);
		GetImageInfo(i);
                break;
            }
 
        default:
            cerr << "Other voxel types not supported" << endl;
        }
    }
    catch(IOException & e)
    {
        cerr << e << endl;
        exit(1);
    }
    catch(InternalException & e)
    {
        cerr << e << endl;
        exit(1);
    }
    catch(bad_alloc &)
    {
        cerr << "Not enough memory." << endl;
        exit(1);
    }
    catch(...)
    {
        cerr << argv[0] << "Unknown exception." << endl;
        exit(1);
    }

    return 0;
}
