/*
 * Tools related to the MitoPacq, the CBIA time-lapse cell observation simulator.
 *
 * Copyright (C) 2005-2013   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library-related tools is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <i3d/image3d.h>
#include <i3d/basic.h>
#include <i3d/filters.h>
#include <i3d/neighbours.h>
#include <vector>
#ifdef _MSC_VER
#include <time.h>
#else
#include <sys/time.h>
#endif
#include <ctype.h>

using namespace std;
using namespace i3d;

template <class VOXEL> void Median (Image3d <VOXEL> &in, Image3d <VOXEL> &out, const Neighbourhood & nb = nb3D_6)
{
  vector <const VOXEL *>win;
  size_t i, j, num;
  i = 0;
  while (i < in.GetImageSize ())
    {
      num = GetWindow (in, in.GetX (i), in.GetY (i), in.GetZ (i), nb, win);
      for (j = 0; j < num; j++)
	{
	  out.SetVoxel (i + j, GetMedian (win));
	  MoveWindow (win);
	}
      i += num;
    }
}

template <class VOXEL> void ApplyMedian(Image3d<VOXEL> & inputImage, Image3d<VOXEL> & outputImage)
{
    if (inputImage.GetSizeZ() == 1)
	Median(inputImage, outputImage, nb2D_4);
    else
	Median(inputImage, outputImage, nb3D_6);

}

int main(int argc, char **argv)
{
    cout << argv[0] << " - part of the tools set of the OpticalFlow library \n"
         "Copyright (C) 2005-2013 Centre for Biomedical Image Analysis (CBIA)\n"
         "This is free software; see the source for copying conditions. "
         "There is NO\nwarranty; not even for MERCHANTABILITY or FITNESS FOR "
         "A PARTICULAR PURPOSE.\n\n";


    if (argc < 3)
    {
        cout << "Bad number of parameters\nUsage: " << argv[0] << " input output\nexample: " << argv[0] << " input.tif output.tif\n";
        return -2;
    }
    ImgVoxelType vt = ReadImageType(argv[1]);
    cerr << "Voxel=" << VoxelTypeToString(vt) << endl;

    switch (vt)
    {
    case FloatVoxel:
        {
            Image3d <float> inputImage(argv[1]);
            Image3d <float> outputImage;
	    outputImage.MakeRoom(inputImage.GetSize());
	    ApplyMedian(inputImage, outputImage);
	    outputImage.SaveImage(argv[2]);
        }
        break;
    case Gray8Voxel:
        {
            Image3d<GRAY8> inputImage(argv[1]);
            Image3d<GRAY8> outputImage;
	    outputImage.MakeRoom(inputImage.GetSize());
	    ApplyMedian(inputImage, outputImage);
	    outputImage.SaveImage(argv[2]);
        }
    case Gray16Voxel:
        {
            Image3d<GRAY16> inputImage(argv[1]);
            Image3d<GRAY16> outputImage;
	    outputImage.MakeRoom(inputImage.GetSize());
	    ApplyMedian(inputImage, outputImage);
	    outputImage.SaveImage(argv[2]);
        }
 
        break;
 
    default:
        cerr << "Other voxel types not supported" << endl;
    }
    return 0;
}
