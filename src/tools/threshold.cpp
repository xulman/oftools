/*
 * Tools related to the MitoPacq, the CBIA time-lapse cell observation simulator.
 *
 * Copyright (C) 2005-2013   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library-related tools is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <iostream>
#include <i3d/image3d.h>
#include <ctype.h>
using namespace std;
using namespace i3d;

template <class V> void DoThreshold(Image3d <V> &i, V lower, V upper)
{
    V *pointer = i.GetVoxelAddr(0);
    V *last = i.GetVoxelAddr(i.GetImageSize());

    while (pointer != last)
    {
        *pointer = ((*pointer >= lower) && (*pointer <= upper) ? std::numeric_limits<V>::max() : std::numeric_limits<V>::min());
        ++pointer;
    }
}

int main(int argc, char *argv[])
{
    cout << argv[0] << " - part of the tools set of the OpticalFlow library \n"
         "Copyright (C) 2005-2013 Centre for Biomedical Image Analysis (CBIA)\n"
         "This is free software; see the source for copying conditions. "
         "There is NO\nwarranty; not even for MERCHANTABILITY or FITNESS FOR "
         "A PARTICULAR PURPOSE.\n\n";

    if ((argc < 5) || (argc > 6))
    {
        cout << "Bad number of parameters\nUsage: " << argv[0] << " input output lower_threshold upper_threshold [b]\nexample: " << argv[0] << " input.tif thresholded.tif 20 90\n";
        cout << "Pixels with intensity value between thresholds are white, otherwise black.\n";
        cout << "The last optional parameter indicates whether the output image should be binary,\n";
        cout << "this works only for GRAY* images. Otherwise, the output image keeps voxel type of the input.\n";
        exit(1);
    }

    try
    {
        // zjisteni typu vstupniho souboru
        // !!! Nefunguje zjisteni souboru z regularniho vyrazu !!!
        cerr << "Reading image Type" << endl;
        ImgVoxelType vt = ReadImageType(argv[1]);
        cerr << "Voxel=" << VoxelTypeToString(vt) << endl;

		  bool binary=(argc == 6)? true : false;

        switch (vt)
        {
        case Gray8Voxel:
            {
                Image3d <GRAY8> i(argv[1]);
                GRAY8 l = atoi(argv[3]), u = atoi(argv[4]);

                DoThreshold(i, l, u);
					 if (binary)
					 {
						 Image3d<bool> bi;
						 i3d::GrayToBinary(i,bi);
						 bi.SaveImage(argv[2]);
					 }
					 else
					 {
					 	i.SaveImage(argv[2]);
					 }
                break;
            }
        case Gray16Voxel:
            {
                Image3d <GRAY16> i(argv[1]);
                GRAY16 l = atoi(argv[3]), u = atoi(argv[4]);

                DoThreshold(i, l, u);
					 if (binary)
					 {
						 Image3d<bool> bi;
						 i3d::GrayToBinary(i,bi);
						 bi.SaveImage(argv[2]);
					 }
					 else
					 {
					 	i.SaveImage(argv[2]);
					 }
                break;
            }
 
        case FloatVoxel:
            {

                Image3d <float>i(argv[1]);
                float l = atof(argv[3]), u = atof(argv[4]);

                DoThreshold(i, l, u);
					 i.SaveImage(argv[2]);
                break;
            }
        default:
            cerr << "Other voxel types not supported" << endl;
        }
    }
    catch(IOException & e)
    {
        cerr << e << endl;
        exit(1);
    }
    catch(InternalException & e)
    {
        cerr << e << endl;
        exit(1);
    }
    catch(bad_alloc &)
    {
        cerr << "Not enough memory." << endl;
        exit(1);
    }
    catch(...)
    {
        cerr << argv[0] << "Unknown exception." << endl;
        exit(1);
    }

    return 0;
}
