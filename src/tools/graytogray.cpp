/*
 * Tools related to the MitoPacq, the CBIA time-lapse cell observation simulator.
 *
 * Copyright (C) 2005-2013   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library-related tools is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <i3d/image3d.h>
#include <i3d/basic.h>

using namespace i3d;
using namespace std;

template <class VOXELIN , class VOXELOUT> void DoCopy  (Image3d<VOXELIN> & inImg, Image3d<VOXELOUT> & outImg)
{
    outImg.MakeRoom(inImg.GetSize());
    outImg.SetOffset(inImg.GetOffset());
    outImg.SetResolution(inImg.GetResolution());

    const size_t iMax = inImg.GetImageSize();
    for (size_t i = 0 ; i < iMax; ++i)
    {
	outImg.SetVoxel(i, static_cast<VOXELOUT>(inImg.GetVoxel(i)));
    }
}

template <class VOXELIN , class VOXELOUT> void DoStretchCopy  (Image3d<VOXELIN> & inImg, Image3d<VOXELOUT> & outImg)
{
    outImg.MakeRoom(inImg.GetSize());
    outImg.SetOffset(inImg.GetOffset());
    outImg.SetResolution(inImg.GetResolution());



    VOXELIN inMin, inMax, diff;
    inImg.GetRange(inMin, inMax);
    diff = inMax-inMin;
    VOXELOUT outMax = std::numeric_limits<VOXELOUT>::max();
    VOXELOUT newVal;

    const size_t iMax = inImg.GetImageSize();
    for (size_t i = 0 ; i < iMax; ++i)
    {
	newVal = static_cast<VOXELOUT>(((static_cast<float>(inImg.GetVoxel(i)) - static_cast<float>(inMin)) / static_cast<float>(diff)) * static_cast<float>(outMax)); 
	outImg.SetVoxel(i, newVal);
    }
}



int main(int argc, char **argv)
{
    cout << argv[0] << " - part of the tools set of the OpticalFlow library \n"
         "Copyright (C) 2005-2013 Centre for Biomedical Image Analysis (CBIA)\n"
         "This is free software; see the source for copying conditions. "
         "There is NO\nwarranty; not even for MERCHANTABILITY or FITNESS FOR "
         "A PARTICULAR PURPOSE.\n\n";


    Image3d <i3d::GRAY8> g8Img;
    Image3d <i3d::GRAY16> g16Img;

    if (argc < 3)
    {
	cout << "The program converts gray8 images to gray16 and vice versa.\n" \
	     << "The type of conversion is decided from the input image.\n";
	cout << "If binary input image is supplied, it is converted to gray8 image;\n" \
	     << "the \"true\" values are converted to the value of 100.\n\n";

        cout << "Bad number of parameters\nUsage: " << argv[0] << " input output [n]\nexample: " << argv[0] << " grayimage1.tif grayimage2.tif\nor\n" << argv[0] << " grayimage1.tif grayimage2.tif n\n\n";
	cout << "The third parameter is whether to stretch/normalize (if the parameter is present)\n" \
	     << "or not to stretch (if not present). It is ignored if binary input image is supplied.\n";
        return -2;
    }
        // zjisteni typu vstupniho souboru
        // !!! Nefunguje zjisteni souboru z regularniho vyrazu !!!
        cerr << "Reading image Type" << endl;
        ImgVoxelType vt = ReadImageType(argv[1]);
        cerr << "Voxel=" << VoxelTypeToString(vt) << endl;

    try
    {
	switch (vt)
	{
	    case BinaryVoxel:
		    {
		    Image3d<bool> Bimg(argv[1]);
		    DoCopy(Bimg,g8Img);
		    g8Img.GetVoxelData()*=100;
		    g8Img.SaveImage(argv[2]);
		    }
		    break;

	    case Gray8Voxel:
		g8Img.ReadImage(argv[1]);
		if (argc == 3)
		    DoCopy(g8Img, g16Img);
		else
		    DoStretchCopy(g8Img, g16Img);
		g16Img.SaveImage(argv[2]);
		break;
	    case Gray16Voxel:
		g16Img.ReadImage(argv[1]);
		if (argc == 3)
		    DoCopy(g16Img, g8Img);
		else
		    DoStretchCopy(g16Img, g8Img);
		g8Img.SaveImage(argv[2]);
		break;

	    default:
		cerr << "Other voxel types not supported!\n";
		break;

	}
    }
    catch(IOException & e)
    {
        cerr << e << endl;
        exit(1);
    }
    catch(InternalException & e)
    {
        cerr << e << endl;
        exit(1);
    }
    catch(bad_alloc &)
    {
        cerr << "Not enough memory." << endl;
        exit(1);
    }
    catch(...)
    {
        cerr << argv[0] << "Unknown exception." << endl;
        exit(1);
    }


    return 0;
}
