/**********************************************************************
* 
* molecule.h
*
* This file is part of MitoGen
* 
* Copyright (C) 2013-2016 -- Centre for Biomedical Image Analysis (CBIA)
* http://cbia.fi.muni.cz/
* 
* MitoGen is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* MitoGen is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with MitoGen. If not, see <http://www.gnu.org/licenses/>.
* 
* Author: David Svoboda and Vladimir Ulman
* 
* Description: A class 'Molecule' is derived from the generic class 'Dot'.
* In MitoGen, each molecule is used to represent the flourescent signals.
*
***********************************************************************/

#ifndef MITOGEN_MOLECULE_H
#define MITOGEN_MOLECULE_H

#include "dots.h"

/**
 * The basic class describing one particular molecule at some time point. The time point is,
 * however, not stored in this class but rather in the Cell class, which is expected
 * to use this class for representation of dot-like structures, e.g. proteins.
 *
 * A single dot is used to simulate one fluorescent molecule in any specimen, hence
 * the name of this class.
 *
 * There is a unique ID assigned to every dot in the scene. This ID is not changing
 * between time points for the given dot as long as the dot exists in the scene. In case the
 * dot is split, e.g. after mitotic cell phase, the parent dot should be removed from the
 * scene and two new dots with new unique IDs will emerge instead. Ofcourse, the new ones
 * will have their \e parentID set apropriately.
 */
class Molecule : public Dot
{
//currently, it is nothing more than ordinary dot...

  public:
			 ///empty/default constructor: inits especially positions to (0,0,0)
			 Molecule() : Dot() {};

			 /// but the explicit constructors must be explicitly defined :-/
			 Molecule(i3d::Vector3d<float> const & POS, const size_t PARENTID=0, const size_t BIRTH=0) : 
						Dot(POS, PARENTID, BIRTH) {};
};

#endif
